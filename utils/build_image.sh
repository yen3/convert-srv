#!/usr/bin/env bash

eval $(docker-machine env leader)
make -f Makefile.local build_image

eval $(docker-machine env node-1)
make -f Makefile.local build_image

eval $(docker-machine env node-2)
make -f Makefile.local build_image
