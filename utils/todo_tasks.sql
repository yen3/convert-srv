-- create types
-- https://stackoverflow.com/questions/7624919/check-if-a-user-defined-type-already-exists-in-postgresql
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'task_status') THEN
        CREATE TYPE task_status AS ENUM('creating', 'ready', 'running', 'done', 'fail');
    END IF;
END$$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'job_status') THEN
        CREATE TYPE job_status AS ENUM('ready', 'running', 'done', 'fail_retry', 'fail');
    END IF;
END$$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'worker_status') THEN
        CREATE TYPE worker_status AS ENUM('ready', 'running', 'fail', 'offline');
    END IF;
END$$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'fail_status') THEN
        CREATE TYPE fail_status AS ENUM('not_run', 'done', 'fatal', 'timeout', 'cancel');
    END IF;
END$$;

-- Task is created by user's requirement
--
-- A task has several jobs. When all jobs of the task are done,
-- the task is done.
CREATE TABLE IF NOT EXISTS task (
    id                   SERIAL PRIMARY KEY,
    name                 text           DEFAULT '',
    input_path           text,
    output_path          text,
    task_type            text,
    cmd                  text,
    cmd_options          text,
    work_dir             text,
    job_retry_limit      integer        DEFAULT 1,
    remove_temp          boolean        DEFAULT true,
    split_unit           integer        DEFAULT 0,
    status               task_status    DEFAULT 'creating',
    fail_reason          fail_status    DEFAULT 'not_run',
    timeout              integer        DEFAULT 0,
    create_time          timestamp      DEFAULT CURRENT_TIMESTAMP,
    start_time           timestamp,
    finish_time          timestamp
);


CREATE TABLE IF NOT EXISTS job (
    id                   SERIAL PRIMARY KEY,
    task_id              integer,
    worker_id            integer,
    input_path           text,
    output_path          text,
    cmd                  text,
    cmd_options          text            DEFAULT '',
    work_dir             text,
    redirect_stdout_err  boolean         DEFAULT true,
    status               job_status      DEFAULT 'ready',
    fail_reason          fail_status     DEFAULT 'not_run',
    timeout              integer         DEFAULT 0,
    create_time          timestamp       DEFAULT CURRENT_TIMESTAMP,
    start_time           timestamp,
    finish_time          timestamp,
    retry_time           integer         DEFAULT 0,
    retry_limit          integer         DEFAULT 1
);


CREATE TABLE IF NOT EXISTS job_log (
    id            SERIAL PRIMARY KEY,
    task_id       integer,
    job_id        integer,
    worker_id     integer,
    retry_time    integer,
    return_code   integer,
    input_path    text,
    output_path   text,
    error_msg     text,
    cmd           text,
    cmd_options   text,
    work_dir      text,
    stdout_log    text,
    stderr_log    text,
    is_timeout    boolean,
    start_time    timestamp,
    finish_time   timestamp
);


CREATE TABLE IF NOT EXISTS worker (
    id                SERIAL PRIMARY KEY,
    name              text,
    addr              text,
    start_time        timestamp             DEFAULT CURRENT_TIMESTAMP,
    last_check_time   timestamp             DEFAULT CURRENT_TIMESTAMP,
    status            worker_status         DEFAULT 'ready'
)
