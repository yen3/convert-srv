#!/bin/bash

./utils/wait-for-it.sh -t 30 redis:6379 -- echo wait redis
./utils/wait-for-it.sh -t 30 postgres:5432 -- echo wait postgres

exec "$@"
