from datetime import datetime
from contextlib import contextmanager
import os
import os.path
import shlex
import copy

from tornado import gen
from sqlalchemy.exc import SQLAlchemyError

from .config import WorkerInfo, app_log
from common import utils
from common.db.job import JobLog, JobStatus, FailStatus
from common.db.worker import WorkerStatus
import common.db.job
import common.db.task
import common.db.worker


@contextmanager
def set_worker_running():
    worker_info = WorkerInfo()
    worker_id = worker_info.id

    common.db.worker.change_worker_status(worker_id,
                                          WorkerStatus.running.value)

    yield

    common.db.worker.change_worker_status(worker_id,
                                          WorkerStatus.ready.value)


@gen.coroutine
def retrieve_new_job():
    """
    Require a new job and assign worker id to the job

    Returns:
        job_id (int) to present a new job
        or None to present no job
    """
    worker_info = WorkerInfo()

    worker_id = worker_info.id
    try:
        job_id = common.db.job.retrieve_new_job(worker_id)
        return job_id
    except SQLAlchemyError as err:
        app_log.error(err)

    return None


def replace_cmd_argumets(s):
    """
    Replace {{var}} string with proper value
    """
    worker_info = WorkerInfo()
    worker_id = worker_info.id

    new_s = s.replace('{{worker_id}}', str(worker_id))

    return new_s


@gen.coroutine
def execute_cmd(job):
    """
    Args:
        job (common.db.job.Job): A Job object

    Returns:
        A dict: {"output_path": str,
                 "stdout": str,
                 "stderr": str,
                 "start_time": datetime.datetime,
                 "finish_time": datetime.datetime,
                 "timeout": bool,
                 "error": str,
                 "return_code": int}
    """
    start_time = datetime.now()

    output_path = replace_cmd_argumets(job.output_path) if job.output_path \
        else None
    cmd = job.cmd
    options = shlex.split(replace_cmd_argumets(job.cmd_options)) \
        if job.cmd_options else []
    cwd = job.work_dir if job.work_dir is not None else None
    redirect = job.redirect_stdout_err
    timeout = None if job.timeout == 0 else job.timeout

    # Create working dictionary if not exists
    try:
        if cwd is not None:
            if not os.path.isdir(cwd):
                os.makedirs(cwd)
    except Exception as error:
        # Maybe other worker create the folder
        pass

    # Start to run the program
    run = yield utils.call(cmd, options,
                           redirect=redirect,
                           cwd=cwd,
                           timeout=timeout)

    end_time = datetime.now()

    return {"output_path": output_path,
            "cmd_options": " ".join(options),
            "return_code": run["return_code"],
            "stdout": run["stdout"],
            "stderr": run["stderr"],
            "timeout": run["timeout"],
            "start_time": start_time,
            "finish_time": end_time,
            "error": run["error"], }


@gen.coroutine
def upload_log(job, result):
    worker_info = WorkerInfo()

    log = JobLog()
    log.task_id = job.task_id
    log.job_id = job.id
    log.worker_id = worker_info.id
    log.retry_time = job.retry_time
    log.input_path = job.input_path
    log.output_path = result['output_path']
    log.cmd = job.cmd
    log.cmd_options = result['cmd_options']
    log.work_dir = job.work_dir
    log.return_code = result['return_code']
    log.is_timeout = result['timeout']
    log.error_msg = result['error']
    log.stdout_log = result['stdout']
    log.stderr_log = result['stderr']
    log.start_time = result['start_time']
    log.finish_time = result['finish_time']

    try:
        new_job_log_id = common.db.job.upload_new_job_log(log)
        return new_job_log_id
    except SQLAlchemyError as err:
        app_log.error(err)

    return None


@gen.coroutine
def update_job_status(job, result):
    worker_info = WorkerInfo()
    worker_id = worker_info.id
    job_id = job.id

    succ = True if result['return_code'] == 0 else False

    # set next_status
    next_status = None
    if succ:
        next_status = JobStatus.done.value
    else:
        next_status = JobStatus.fail_retry.value

        if job.retry_time == job.retry_limit:
            next_status = JobStatus.fail.value

    # set fail reason
    fail_reason = None
    if next_status == JobStatus.done.value:
        fail_reason = FailStatus.done.value
    elif next_status in [JobStatus.fail.value, JobStatus.fail_retry.value]:
        if result['timeout']:
            fail_reason = FailStatus.timeout.value
        else:
            fail_reason = FailStatus.fatal.value

    finish_time = result['finish_time']

    if next_status == JobStatus.done.value:
        output_path = result['output_path']
        cmd_options = result['cmd_options']
    else:
        output_path = job.output_path
        cmd_options = job.cmd_options

    try:
        common.db.job.update_job_status(job_id, worker_id,
                                        status=next_status,
                                        fail_reason=fail_reason,
                                        finish_time=finish_time,
                                        output_path=output_path,
                                        cmd_options=cmd_options)
        return True
    except SQLAlchemyError as err:
        app_log.error(err)

    return False


@gen.coroutine
def run_job_internal(job_id):
    job = common.db.job.get_job(job_id)

    result = yield execute_cmd(job)

    log_id = yield upload_log(job, result)
    if log_id:
        app_log.info("Log job {job_id} in jog_log {job_log_id}".format(
            job_id=job.id, job_log_id=log_id))

    is_update = yield update_job_status(job, result)
    if is_update is False:
        app_log.error("Update job {job_id} failed.".format(job_id=job.id))


@gen.coroutine
def run_job():
    job_id = yield retrieve_new_job()

    if job_id:
        common.db.task.refresh_task_start_time(job_id)

        app_log.info("Receive a job {job_id}".format(job_id=job_id))

        with set_worker_running():
            yield run_job_internal(job_id)

        app_log.info("Finish the job {job_id}".format(job_id=job_id))
    else:
        # No new job
        yield gen.sleep(1)


@gen.coroutine
def run_job_daemon():
    # Check worker register status
    worker_info = WorkerInfo()
    while worker_info.id is None:
        yield gen.sleep(1)

    # Start to receive jobs
    while True:
        yield run_job()
