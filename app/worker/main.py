import logging
import signal

from tornado.ioloop import IOLoop
from tornado_json.application import Application
import tornado.ioloop
import tornado.httpserver
import tornado.log

from common.logging import init_log
from common.db.worker import register_current_worker
from common.db.worker import unregister_worker
from worker.api import PingHandler
from worker.config import WorkerInfo
from worker.config import WORKER_PORT
from worker.daemon import run_job_daemon


def sig_handler(sig, frame):
    logging.info('Server shutdown ...')
    IOLoop.current().stop()


def init_sig_handler():
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)


def init_daemons():
    daemon_funs = [run_job_daemon]

    io_loop = IOLoop.instance()
    for fun in daemon_funs:
        io_loop.spawn_callback(fun)


def init_app():
    routes = [(r"/api/v1/", PingHandler), ]
    return Application(routes=routes, settings={'debug': True})


def main():
    init_log()
    init_sig_handler()
    init_daemons()

    # Config the server
    app = init_app()

    # Register the current worker to database
    worker_info = WorkerInfo()
    worker_info.id = register_current_worker()
    logging.info('Register worker id: {worker_id}'.format(
        worker_id=worker_info.id))

    # Start the server
    logging.info('Server started on port %s ...' % (WORKER_PORT,))

    server = tornado.httpserver.HTTPServer(app)
    server.listen(WORKER_PORT)
    try:
        IOLoop.instance().start()
    finally:
        logging.info("Terminate the worker and unregister "
                     "worker_id: {worker_id}".format(worker_id=worker_info.id))
        unregister_worker(worker_info.id)
