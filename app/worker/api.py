from tornado_json.requesthandlers import APIHandler
from tornado_json import schema
from tornado.web import MissingArgumentError


class PingHandler(APIHandler):
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self._check_num = 0

    @schema.validate(output_schema={"type": "string"})
    def get(self):
        return "worker is running"
