import logging

app_log = logging.getLogger("tornado.application")

WORKER_PORT = 11000


class WorkerInfo(object):
    class _WorkerInfo(object):
        def __init__(self):
            self.id = None

    instance = None

    def __init__(self):
        if not WorkerInfo.instance:
            WorkerInfo.instance = WorkerInfo._WorkerInfo()

    @property
    def id(self):
        return WorkerInfo.instance.id

    @id.setter
    def id(self, set_id):
        WorkerInfo.instance.id = set_id
