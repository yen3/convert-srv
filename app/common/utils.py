import datetime
import logging
import shlex
import traceback
import os.path

from tornado.process import Subprocess
from tornado.gen import TimeoutError
from tornado import gen

app_log = logging.getLogger("tornado.application")


def utf8_str(s, force=True):
    if isinstance(s, bytes):
        return s.decode('utf8')
    elif isinstance(s, str):
        return s
    elif force is True:
        return str(s)
    else:
        raise NotImplementedError("Can not convert: " + str(s))


def dict_str_b2s(d):
    return {utf8_str(k, force=False): utf8_str(v, force=False)
            for k, v in d.items()}


def file_suffix(path):
    try:
        return os.path.splitext(os.path.basename(path))[1]
    except:
        return ""


def path_without_suffix(path):
    if path == "":
        return path

    try:
        dirpath = os.path.dirname(path)
        basename = os.path.basename(path)
        basename_without_suffix = os.path.splitext(basename)[0]

        if not dirpath:
            return basename_without_suffix
        else:
            return os.path.join(dirpath, basename_without_suffix)

    except:
        return ""


@gen.coroutine
def call(cmd, options, redirect=True, shell=False, cwd=None, timeout=None):
    """
    Executing a command with options

    Args:
        cmd (str): program
        options ([str]): command options
        redirect (bool): default True, return stdout and stderr
        timeout (int): program's execution time limit. unit: sec(s)

    Returns:
        A dict {"return_code": int,
                "timeout": bool,
                "stdout": str,
                "stderr": str,
                "error": str}

    References:
       1. https://gist.github.com/FZambia/5756470
       2. https://stackoverflow.com/questions/39251682/tornado-with-timeout-correct-usage
    """
    if isinstance(options, str):
        options = shlex.split(options)

    # Prepare Subprocess
    stdout = Subprocess.STREAM if redirect else None
    stderr = Subprocess.STREAM if redirect else None

    try:
        args = [cmd]
        args.extend(options)
        proc = Subprocess(args=args, shell=shell, cwd=cwd,
                          stdout=stdout, stderr=stderr)
    except OSError as err:
        app_log.error(err)
        app_log.debug(traceback.format_exc())
        return {"return_code": -1, "timeout": False,
                "stdout": "",
                "stderr": "",
                "error": type(err).__name__ + " - " + str(err), }

    # Run the command
    return_code = -1
    try:
        if timeout is None:
            return_code = yield proc.wait_for_exit(raise_error=False)
        else:
            return_code = yield gen.with_timeout(
                datetime.timedelta(seconds=timeout),
                proc.wait_for_exit(raise_error=False))

    except TimeoutError as err:
        app_log.error(err)
        app_log.error(traceback.format_exc())
        return {"return_code": -1, "timeout": True,
                "stdout": "", "stderr": "", "error": "process timeout"}
    except OSError as err:
        app_log.error(err)
        app_log.debug(traceback.format_exc())
        return {"return_code": -1, "timeout": False,
                "stdout": "", "stderr": "",
                "error": type(err).__name__ + " - " + str(err)}

    # Read stdout and stderr
    result, error = "", ""
    if redirect:
        result, error = yield [
            gen.Task(proc.stdout.read_until_close),
            gen.Task(proc.stderr.read_until_close)
        ]

    result = utf8_str(result)
    error = utf8_str(error)
    return {"return_code": return_code, "timeout": False,
            "stdout": result, "stderr": error, "error": ""}
