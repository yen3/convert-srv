import itertools
import functools
import types
import logging
import json

from tornado import gen
import tornado.ioloop
import tornadis


class RedisClient(object):
    def __init__(self, *args, **kwargs):
        self._client = tornadis.Client(*args, **kwargs)

    @gen.coroutine
    def connect(self):
        rtv = yield self._client.connect()
        return rtv

    @gen.coroutine
    def disconnect(self):
        rtv = self._client.disconnect()
        return rtv

    @gen.coroutine
    def ping(self):
        rtv = yield self._client.call('PING')
        return rtv.decode('utf8')

    @gen.coroutine
    def flushall(self):
        rtv = yield self._client.call('FLUSHALL')
        return rtv

    @gen.coroutine
    def delete(self, *keys):
        rtv = yield self._client.call('DEL', *keys)
        return rtv

    @gen.coroutine
    def hset(self, key, field, value):
        rtv = yield self._client.call('HSET', key, field, value)
        return rtv

    @gen.coroutine
    def hget(self, key, field):
        rtv = yield self._client.call('HGET', key, field)
        return rtv.decode('utf8')

    @gen.coroutine
    def keys(self, key):
        keys = yield self._client.call("KEYS", key)
        return [k.decode('utf8') for k in keys]

    @gen.coroutine
    def hmset(self, key, data):
        """
        Args:
            data: dict. The key and value of the dict must be the str type.
        """
        args = list(itertools.chain(*[[k, v] for k, v in data.items()]))
        rtv = yield self._client.call('HMSET', key, *args)
        return rtv.decode('utf8')

    @gen.coroutine
    def hgetall(self, key):
        """
        Returns:
            a dict: The types of key and value are str
        """
        raw_data = yield self._client.call('HGETALL', key)
        raw_data = [d.decode('utf8') for d in raw_data]

        return dict(zip(raw_data[::2], raw_data[1::2]))


class RedisPublisher(RedisClient):
    def __init__(self, channel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._channel = channel

    @gen.coroutine
    def publish(self, msg, is_json=False):
        """
        Publish message to redis channel

        Args:
            msg (string, byte): send message
            is_json: if True, dump the msg with json format to string type
        """
        if is_json:
            msg = json.dumps(msg)

        send_msg = msg.encode('utf8') if isinstance(msg, str) else msg
        rtv = yield self._client.call('PUBLISH', self._channel, msg)
        return rtv

    @gen.coroutine
    def publish_json(self, msg):
        rtv = yield self.publish(msg, is_json=True)
        return rtv


class RedisSubscriptor(object):
    def __init__(self, channel, *args, **kwargs):
        self._channel = channel
        self._client = tornadis.PubSubClient(*args, **kwargs)

    @gen.coroutine
    def connect(self):
        self._client.connect()
        rtv = yield self._sub_channel()
        return rtv

    @gen.coroutine
    def disconnect(self):
        rtv = self._client.disconnect()
        return rtv

    @gen.coroutine
    def _sub_channel(self):
        rtv = yield self._client.pubsub_subscribe(self._channel)
        return rtv

    @gen.coroutine
    def subscribe(self, is_json=False, sleep_secs=3):
        """
        Subscribe the message from redis channel

        Args:
            is_json: if True, decodes the message with json

        Return: A dict with three fields
           {'type': 'message',
            'channel': 'the:send:channel',
            'message': 'the send message'}

        Example:
            {'type': 'message',
             'channel': 'qconvert:test',
             'message': 'Hello World'}

            # With `is_json=True`
            {'type': 'message',
             'channel': 'qconvert:test',
             'message': {'id': 1, 'msg': 'Hello World'}}
        """
        msg_format_key = ['type', 'channel', 'message']

        raw_msg = yield self._client.pubsub_pop_message()
        if isinstance(raw_msg, tornadis.TornadisException):
            logging.debug("Waitting for new event")
            yield gen.sleep(sleep_secs)
            yield self._sub_channel()
            return raw_msg

        msg = dict(zip(msg_format_key, [m.decode('utf8') for m in raw_msg]))

        if is_json:
            msg['message'] = json.loads(msg['message'])

        return msg

    @gen.coroutine
    def subscribe_json(self, sleep_secs=3):
        rtv = yield self.subscribe(is_json=True, sleep_secs=sleep_secs)
        return rtv


# TODO: Check the class can be used in real environment or NOT.
class RedisClientConextManager(object):
    def __init__(self, *args, **kwargs):
        self.client = RedisClient(*args, **kwargs)

    def __enter__(self):
        tornado.ioloop.IOLoop.current().add_future(self.client.connect(),
                                                   lambda f: None)
        return self.client

    def __exit__(self, exc_type, exc_value, traceback):
        tornado.ioloop.IOLoop.current().add_future(self.client.disconnect(),
                                                   lambda f: None)
        if exc_value is not None:
            logging.debug(str(traceback))
            raise exc_type(exc_value)
