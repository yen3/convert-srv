from datetime import datetime
from enum import Enum
import os.path
import logging

from sqlalchemy.sql.expression import or_, and_
from sqlalchemy.exc import SQLAlchemyError

from .general import Task, session_context
from .general import object_to_dict
from common.db.job import Job, JobLog


class TaskStatus(Enum):
    creating = 'creating'
    ready = 'ready'
    running = 'running'
    done = 'done'
    timeout = 'timeout'
    fail = 'fail'


class FailStatus(Enum):
    not_run = 'not_run'
    done = 'done'
    fatal = 'fatal'
    timeout = 'timeout'


def task_dict(task):
    return object_to_dict(task, Task)


def get_task(task_id):
    if task_id is None:
        return None

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).first()
        session.flush()
        if task is not None:
            session.expunge(task)

        session.commit()

    return task


def get_all_task_ids():
    with session_context() as session:
        raw_ids = session.query(
            Task.id.label('task_id')).order_by(Task.id).all()
        session.flush()
        task_ids = [r.task_id for r in raw_ids]
        session.commit()

        return task_ids


def get_task_status(task_id):
    if task_id is None:
        return None

    try:
        status = None
        with session_context() as session:
            query = session.query(Task.status.label('task_status'))
            query = query.filter(Task.id == task_id)
            task = query.one()
            session.flush()

            status = task.task_status

            session.commit()
    except Exception as err:
        logging.error(err)
    finally:
        return status


def change_task_status(task_id, next_status):
    set_finished_time = False
    if next_status in [TaskStatus.done.value, TaskStatus.fail.value]:
        set_finished_time = True

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).one()
        task.status = next_status

        if set_finished_time:
            task.finish_time = datetime.now()

        session.commit()


def refresh_task_start_time(job_id):
    try:
        with session_context() as session:
            task_id = session.query(Job.task_id.label('task_id')).filter(
                    Job.id == job_id).first().task_id
            session.flush()

            task = session.query(Task).filter(
                and_(Task.id == task_id,
                     Task.start_time == None)).with_for_update().first()

            if task:
                task.start_time = datetime.now()
                session.commit()


    except (AttributeError, SQLAlchemyError) as err:
        logging.warning(err)


def create_task(task_work_dir_prefix=None, **attrs):
    """
    Create a new task

    Returns:
        a integer to present a task id
    """
    check_attrs = ['input_path', 'output_path',
                   'cmd', 'cmd_options', 'task_type']

    for check_attr in check_attrs:
        if check_attr not in attrs:
            logging.error("Create task fail with no " + check_attr + " field")
            return None

    work_dir_prefix = task_work_dir_prefix if task_work_dir_prefix else "/tmp"

    task_id = None
    with session_context() as session:
        task = Task(**attrs)
        session.add(task)
        session.commit()

        task_id = task.id

    if task_id is None:
        return None

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).one()
        work_dir = os.path.join(work_dir_prefix,
                                "task_{task_id}".format(task_id=task_id))
        task.work_dir = work_dir
        session.commit()

    return task_id


def delete_task(task_id):
    if task_id is None:
        return False

    # Delete all records
    try:
        with session_context() as session:
            session.query(Task).filter(Task.id == task_id).delete()
            session.query(Job).filter(Job.task_id == task_id).delete()
            session.query(JobLog).filter(JobLog.task_id == task_id).delete()

            session.commit()
        return True
    except Exception as err:
        logging.error(err)
        return False
