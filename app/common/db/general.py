from contextlib import contextmanager
import logging
import traceback

import sqlalchemy.types
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

__all__ = ['URL', 'Task', 'Job', 'JobLog', 'Worker', 'Session',
           'session_context']

URL = "postgres+psycopg2://postgres:@postgres:5432"
ENGINE = create_engine(URL)

Base = automap_base()
Base.prepare(ENGINE, reflect=True)

Session = sessionmaker(bind=ENGINE)
Task = Base.classes.task
Job = Base.classes.job
JobLog = Base.classes.job_log
Worker = Base.classes.worker


@contextmanager
def session_context():
    s = Session()

    try:
        yield s
    except Exception as err:
        logging.error(err)
        logging.debug(traceback.format_exc())
        s.rollback()
        raise err
    finally:
        s.close()


def object_to_dict(obj, ObjType):
    def convert_datetime(value):
        if value:
            return value.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return ""

    obj_dict = {}
    for col in ObjType.__table__.columns:
        if isinstance(col.type, sqlalchemy.types.DateTime):
            value = convert_datetime(getattr(obj, col.name))
        elif isinstance(col.type, sqlalchemy.types.Numeric):
            value = int(getattr(obj, col.name))
        else:
            value = getattr(obj, col.name)

        obj_dict[col.name] = value

    return obj_dict
