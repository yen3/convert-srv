from datetime import datetime
from enum import Enum
import logging

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql.expression import or_, and_

from .general import Job, JobLog
from .general import Session, session_context
from .general import object_to_dict


class JobStatus(Enum):
    ready = 'ready'
    running = 'running'
    done = 'done'
    fail = 'fail'
    fail_retry = 'fail_retry'


class FailStatus(Enum):
    not_run = 'not_run'
    done = 'done'
    fatal = 'fatal'
    timeout = 'timeout'
    cancel = 'cancel'


def job_dict(job):
    return object_to_dict(job, Job)


def job_log_dict(job_log):
    return object_to_dict(job_log, JobLog)


def retrieve_new_job(worker_id):
    """
    Args:
        worker_id (int): for locking the required job

    Returns:
        A job_id(int) to present a new job
        or None to present no job
    """
    new_job_id = None

    with session_context() as session:
        # create query
        query = session.query(Job)
        query = query.filter(or_(Job.status == JobStatus.fail_retry.value,
                                 Job.status == JobStatus.ready.value))
        query = query.order_by(Job.id).limit(1).with_for_update()

        job = query.first()

        # New job is comming
        if job is not None:
            job.worker_id = worker_id
            job.start_time = datetime.now()
            job.status = JobStatus.running.value
            job.retry_time = job.retry_time + 1
            session.flush()

            new_job_id = job.id

        session.commit()

    return new_job_id


def get_job(job_id):
    if job_id is None:
        return None

    with session_context() as session:
        job = session.query(Job).filter(Job.id == job_id).first()

        session.flush()
        if job is not None:
            session.expunge(job)
        session.commit()

    return job


def get_jobs_by_task_id(task_id):
    if task_id is None:
        return []

    with session_context() as session:
        query = session.query(Job).filter(Job.task_id == task_id)
        query = query.order_by(Job.id)
        jobs = query.all()
        session.flush()

        for job in jobs:
            session.expunge(job)

        session.commit()

    return jobs


def update_job_status(job_id, worker_id, **attrs):
    """
    Args:
        job_id (int): a Job id
        worker_id (int): for checking the job is not rewwrited by server

    Returns:
        True -- Success
        False -- The job is resetting by the server, ignore this run
    """
    with session_context() as session:
        query = session.query(Job)
        query = query.filter(and_(Job.id == job_id,
                                  Job.worker_id == worker_id))
        query = query.with_for_update()

        job = query.first()

        if job is not None:
            for k, v in attrs.items():
                setattr(job, k, v)

        session.commit()

        return True if job is not None else False

    return False


def upload_new_job_log(job_log):
    """
    Args:
        job_log (JobLog): a JobLog object
    """
    if job_log.finish_time is None:
        job_log.finish_time = datetime.now()

    with session_context() as session:
        session = Session()
        session.add(job_log)
        session.commit()

        return job_log.id

    raise AttributeError("Add a row fail: job log infomation to database")
