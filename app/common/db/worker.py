from enum import Enum
from datetime import datetime
from socket import gethostname, gethostbyname
import logging

from sqlalchemy.exc import SQLAlchemyError

from .general import Worker
from .general import Session, session_context
from .general import object_to_dict


class WorkerStatus(Enum):
    ready = 'ready'
    running = 'running'
    fail = 'fail'
    offline = 'offline'


def worker_dict(worker):
    return object_to_dict(worker, Worker)


def get_worker(worker_id):
    if worker_id is None:
        return None

    try:
        with session_context() as session:
            worker = session.query(Worker).filter(Worker.id == worker_id).one()
            session.flush()

            if worker is not None:
                session.expunge(worker)

            session.commit()

            return worker

    except Exception as err:
        logging.error(err)
        return None

    return None


def get_all_workers():
    try:
        with session_context() as session:
            workers = session.query(Worker).order_by(Worker.id).all()
            session.flush()

            for worker in workers:
                session.expunge(worker)

            return workers

    except Exception as err:
        logging.error(err)
        return []

    return []


def get_all_worker_ids():
    try:
        with session_context() as session:
            query = session.query(Worker.id.label('worker_id')).order_by(Worker.id)
            workers = query.all()
            session.commit()

            return [w.worker_id for w in workers]

    except Exception as err:
        logging.error(err)
        return []

    return []


def register_current_worker(name=None, addr=None, start_time=None):
    """
    Get the current host status and write to database
    """
    name = gethostname() if name is None else name
    addr = gethostbyname(name) if addr is None else addr
    start_time = datetime.now() if start_time is None else start_time

    with session_context() as session:
        w = Worker(name=name, addr=addr, start_time=start_time)
        session.add(w)
        session.commit()

        return w.id

    raise AttributeError("Add a row fail: worker infomation to database")


def change_worker_status(worker_id, next_status, origin_status=None):
    """
    Change the worker status to next status

    Args:
        worker_id (int)
        next_status (str)
        origin_status (str)
    """
    check_status = None

    if origin_status is None:
        check_status = [WorkerStatus.ready.value, WorkerStatus.running.value]
    else:
        check_status = [origin_status]

    try:
        with session_context() as session:
            w = session.query(Worker).filter(
                Worker.id == worker_id).with_for_update().one()

            if w.status in check_status:
                w.status = next_status
                w.last_check_time = datetime.now()

            session.commit()
            return True

    except SQLAlchemyError as error:
        logging.error(error)
        return False


def unregister_worker(worker_id):
    """
    Change the worker status to done

    Args:
        worker_id (int)
    """
    return change_worker_status(worker_id, WorkerStatus.offline.value)


def renew_worker_check_time(worker_id, update_time=None):
    """
    Args:
        worker_id (int)
        time (datetime.datetime)
    """
    update_time = datetime.now() if update_time is None else update_time

    with session_context() as session:
        w = session.query(Worker).filter(Worker.id == worker_id).one()
        w.last_check_time = update_time

        session.commit()
