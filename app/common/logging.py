import logging

import tornado.log


class LogFormatter(tornado.log.LogFormatter):
    def __init__(self):
        super(LogFormatter, self).__init__(
            fmt='%(color)s[%(levelname)1.1s %(asctime)s %(module)s:%(lineno)d]%(end_color)s %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )


def init_log():
    tornado.log.enable_pretty_logging()

    for name in ['tornado.access', 'tornado', 'tornado.application',
                 'tornado.general']:
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)

    for handler in logging.getLogger().handlers:
        handler.setFormatter(LogFormatter())
