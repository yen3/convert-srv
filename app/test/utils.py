from datetime import datetime

from tornado.escape import json_decode
from tornado import gen


@gen.coroutine
def fetch_url(http_client, url):
    raw_response = yield http_client.fetch(url)
    res = json_decode(raw_response.body.decode('utf8'))
    assert res['status'] == 'success'
    return res['data']


def datetime_now():
    n = datetime.now()

    return datetime(n.year, n.month, n.day,
                    hour=n.hour, minute=n.minute, second=n.second)


def datetime_equal(x, y, comp_second=True):
    if x.year != y.year:
        return False
    if x.month != y.month:
        return False
    if x.day != y.day:
        return False
    if x.hour != y.hour:
        return False
    if x.minute != y.minute:
        return False
    if comp_second:
        if x.second != y.second:
            return False
    return True
