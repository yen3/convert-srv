from datetime import datetime
import shutil
import os
import os.path

import pytest

from common.db.general import session_context
from common.db.task import Task, TaskStatus, get_task
from common.db.job import Job, JobStatus, FailStatus, get_jobs_by_task_id
from api_server.ffmpeg.task import init_task, merge_task
from api_server.ffmpeg.task import _calculate_split_unit
from api_server.ffmpeg.task import _generate_split_cmd
from api_server.ffmpeg.task import _generate_split_cmd_output_format


@pytest.yield_fixture
def test_task(db_server):
    task = get_task(2)

    if os.path.isdir(task.work_dir):
        shutil.rmtree(task.work_dir)

    yield task

    if os.path.isdir(task.work_dir):
        shutil.rmtree(task.work_dir)


def insert_test_merge_data():
    task_data = [
        {"name": "test_task_1",
         "input_path": "/mnt/data/trailer.mp4",
         "output_path": "/mnt/data/trailer_output.mp4",
         "work_dir": "/mnt/data/workspace/task_2",
         "task_type": "ffmpeg",
         "cmd": "ffmpeg",
         "cmd_options": "-c:v libx264 -c:a copy",
         "remove_temp": False,
         "status": "running",
        },
    ]

    job_data = [
        {"task_id": 2,
         "worker_id": 1,
         "input_path": "/mnt/data/workspace/task_2/task_2_input_0000000000.mp4",
         "output_path": "/mnt/data/workspace/task_2/task_2_0000000000_1.mp4",
         "cmd": "ffmpeg",
         "cmd_options": "-i /mnt/data/workspace/task_2/task_2_input_0000000000.mp4 -c:v libx264 -c:a copy /mnt/data/workspace/task_2/task_2_0000000000_1.mp4",
         "work_dir": "/mnt/data/workspace/task_2",
         "redirect_stdout_err": True,
         "status": "done",
         "fail_reason": "done",
         "timeout": 0,
         "start_time": datetime.now(),
         "finish_time": datetime.now(),
         "retry_time": 1,
         "retry_limit": 1,
        },
        {"task_id": 2,
         "worker_id": 2,
         "input_path": "/mnt/data/workspace/task_2/task_2_input_0000000001.mp4",
         "output_path": "/mnt/data/workspace/task_2/task_2_0000000001_2.mp4",
         "cmd": "ffmpeg",
         "cmd_options": "-i /mnt/data/workspace/task_2/task_2_input_0000000001.mp4 -c:v libx264 -c:a copy /mnt/data/workspace/task_2/task_2_0000000001_2.mp4",
         "work_dir": "/mnt/data/workspace/task_2",
         "redirect_stdout_err": True,
         "status": "done",
         "fail_reason": "done",
         "timeout": 0,
         "start_time": datetime.now(),
         "finish_time": datetime.now(),
         "retry_time": 1,
         "retry_limit": 1,
        },
        {"task_id": 2,
         "worker_id": 3,
         "input_path": "/mnt/data/workspace/task_2/task_2_input_0000000002.mp4",
         "output_path": "/mnt/data/workspace/task_2/task_2_0000000002_3.mp4",
         "cmd": "ffmpeg",
         "cmd_options": "-i /mnt/data/workspace/task_2/task_2_input_0000000002.mp4 -c:v libx264 -c:a copy /mnt/data/workspace/task_2/task_2_0000000002_3.mp4",
         "work_dir": "/mnt/data/workspace/task_2",
         "redirect_stdout_err": True,
         "status": "done",
         "fail_reason": "done",
         "timeout": 0,
         "start_time": datetime.now(),
         "finish_time": datetime.now(),
         "retry_time": 1,
         "retry_limit": 1,
        },
    ]

    tasks = [Task(**t) for t in task_data]
    jobs = [Job(**j) for j in job_data]

    with session_context() as session:
        session.add_all(tasks)
        session.add_all(jobs)
        session.commit()


@pytest.mark.parametrize("unit,expected",[
    (0, 300),
    (10, 10),
    (100, 100),
])
def test_calculate_split_unit(unit, expected):
    assert _calculate_split_unit(unit) == expected


def test_generate_split_cmd():
    input_path = "./test_not_exist.mp4"
    output_format = "test_not_exist_input_%010d.mp4"
    split_unit = 10

    assert _generate_split_cmd(input_path, output_format, split_unit) \
            == ('ffmpeg -i ./test_not_exist.mp4 -map 0 '
                '-c:v copy -c:a copy '
                '-f segment '
                '-segment_time 10 '
                '-v error '
                '-y "test_not_exist_input_%010d.mp4"')


def test_generate_split_cmd_output_format():
    task_id = 2
    input_path = "./test_not_exist.mp4"
    work_dir = "/mnt/data/workspace/task_2"

    assert _generate_split_cmd_output_format( task_id, input_path, work_dir) \
            == ("task_2_input",
                "/mnt/data/workspace/task_2/task_2_input_%010d.mp4")


@pytest.yield_fixture
def test_done_task(db):
    # Insert fake data
    insert_test_merge_data()

    # Get the task
    task = get_task(2)

    # Copy test file
    if os.path.isdir(task.work_dir):
        shutil.rmtree(task.work_dir)

    if os.path.isfile(task.output_path):
        os.remove(task.output_path)

    os.makedirs(task.work_dir)
    shutil.copyfile('/mnt/data/task_2_0000000000_1.mp4',
                    '/mnt/data/workspace/task_2/task_2_0000000000_1.mp4')
    shutil.copyfile('/mnt/data/task_2_0000000001_2.mp4',
                    '/mnt/data/workspace/task_2/task_2_0000000001_2.mp4')
    shutil.copyfile('/mnt/data/task_2_0000000002_3.mp4',
                    '/mnt/data/workspace/task_2/task_2_0000000002_3.mp4')

    # Yield the fake task
    yield task

    # Clean the workspace
    if os.path.isdir(task.work_dir):
        shutil.rmtree(task.work_dir)

    if os.path.isfile(task.output_path):
        os.remove(task.output_path)


@pytest.mark.slow
def test_ffmpeg_init_task(test_task):
    task = test_task
    task_id = task.id

    init_status = init_task(task)
    assert init_status is True

    # Check task's status
    task = get_task(task_id)

    # Get all jobs of the task
    with session_context() as session:
        jobs = session.query(Job).filter(Job.task_id == task.id).all()
        session.flush()

        for job in jobs:
            session.expunge(job)

        session.commit()

    assert len(jobs) == 17
    for n, job in enumerate(jobs):
        assert job.task_id == task_id
        assert job.work_dir == task.work_dir
        assert job.cmd == task.cmd

        check_path = "/mnt/data/workspace/task_2/task_2_input_%010d.mp4" % n
        assert job.input_path == check_path

        check_path = "/mnt/data/workspace/task_2/task_2_%010d_{{worker_id}}.mp4" % n
        assert job.output_path == check_path

        check_options = (
             '-i /mnt/data/workspace/task_2/task_2_input_%010d.mp4 '
             '-c:v libx264 -c:a copy '
             '/mnt/data/workspace/task_2/task_2_%010d_{{worker_id}}.mp4'
        ) % (n, n)
        assert task.cmd_options in check_options
        assert job.cmd_options == check_options

        assert job.retry_limit == 1
        assert job.retry_time == 0
        assert job.status == JobStatus.ready.value
        assert job.timeout == 0
        assert job.fail_reason == FailStatus.not_run.value
        assert job.start_time is None
        assert job.finish_time is None


@pytest.mark.slow
def test_ffmpeg_merge_task(test_done_task):
    task = test_done_task
    task_id = task.id

    merge_status = merge_task(task)
    assert merge_status is True

    task = get_task(task_id)
    assert os.path.isfile(task.output_path)

    if task.remove_temp:
        assert not os.path.isdir(task.work_dir)
    else:
        # Check file list
        jobs = get_jobs_by_task_id(task_id)

        assert os.path.isfile(os.path.join(task.work_dir, "file_list.txt"))
        with open(os.path.join(task.work_dir, "file_list.txt")) as fin:
            contents = fin.read()
            for job in jobs:
                assert job.output_path in contents

