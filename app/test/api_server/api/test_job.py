import os.path

import pytest

from test.utils import fetch_url


@pytest.fixture
def job_base_url(base_url):
    return os.path.join(base_url, "api/v1/job/")


@pytest.fixture
def joblog_base_url(base_url):
    return os.path.join(base_url, "api/v1/joblog/")


@pytest.mark.gen_test
def test_get_job(http_client, db_server, job_base_url):
    # TODO: finish the test
    url = os.path.join(job_base_url, "2")
    data = yield fetch_url(http_client, url)
    print(data)


@pytest.mark.gen_test
def test_get_jobs_by_task_id(http_client, db_server, base_url):
    url = os.path.join(base_url, "api/v1/task/2/job")
    data = yield fetch_url(http_client, url)
    print(data)


@pytest.mark.gen_test
def test_get_joblog(http_client, db_server, joblog_base_url):
    # TODO: finish the test
    url = os.path.join(joblog_base_url, "2")
    data = yield fetch_url(http_client, url)
    print(data)


@pytest.mark.gen_test
def test_get_job_logs_by_job_id(http_client, db_server, base_url):
    # TODO: finish the test
    url = os.path.join(base_url, "api/v1/job/2/log")
    data = yield fetch_url(http_client, url)
    print(data)
