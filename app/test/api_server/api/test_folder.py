import os
import os.path
import shutil
import pprint

import pytest

from tornado.httpclient import HTTPRequest
from tornado.escape import json_decode, url_escape


@pytest.fixture
def folder_base_url(base_url):
    return os.path.join(base_url, "api/v1/folder/")


@pytest.mark.gen_test
def test_get_folder_file_list_root(http_client, folder_base_url,
                                   mocker, monkeypatch):
    # Mock `os.listdir` and `os.path.isdir`
    def fake_isdir(path):
        return path == 'workspace'
    monkeypatch.setattr(os.path, 'isdir', fake_isdir)

    fake_listdir = ['trailer.mp4', 'trailer_short.mp4', 'workspace']
    mocker.patch.object(os, 'listdir')
    os.listdir.return_value = fake_listdir

    # Send request
    url = folder_base_url
    request = HTTPRequest(url=url, method="GET")
    response = yield http_client.fetch(request)
    data = json_decode(response.body.decode('utf8'))
    assert data['status'] == 'success'

    # Validate the response
    folder_data = data['data']
    folder_data.sort(key=lambda entry: entry['name'])
    assert folder_data == [
        {'is_dir': False, 'name': 'trailer.mp4', 'path':
         '/trailer.mp4'},
        {'is_dir': False, 'name': 'trailer_short.mp4',
         'path': '/trailer_short.mp4'},
        {'is_dir': False, 'name': 'workspace', 'path':
         '/workspace'}
    ]


@pytest.mark.gen_test
def test_get_folder_file_list_with_path(http_client, folder_base_url,
                                        mocker, monkeypatch):
    # Mock `os.listdir` and `os.path.isdir`
    def fake_isdir(path):
        return path == 'workspace'
    monkeypatch.setattr(os.path, 'isdir', fake_isdir)

    fake_listdir = ['task_2_0000000000_1.mp4', 'task_2_0000000000_2.mp4']
    mocker.patch.object(os, 'listdir')
    os.listdir.return_value = fake_listdir

    # Send request
    url = os.path.join(folder_base_url, url_escape('workspace'))
    request = HTTPRequest(url=url, method="GET")
    response = yield http_client.fetch(request)
    data = json_decode(response.body.decode('utf8'))

    # Validate response
    assert data == {
        'data': [
            {'is_dir': False,
             'name': 'task_2_0000000000_1.mp4',
             'path': '/workspace/task_2_0000000000_1.mp4'},
            {'is_dir': False,
             'name': 'task_2_0000000000_2.mp4',
             'path': '/workspace/task_2_0000000000_2.mp4'}],
        'status': 'success'
    }

