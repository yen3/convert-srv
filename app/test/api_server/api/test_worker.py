import os.path

from tornado.httpclient import AsyncHTTPClient, HTTPRequest
import pytest

from test.utils import fetch_url


@pytest.fixture
def worker_base_url(base_url):
    return os.path.join(base_url, "api/v1/worker")


@pytest.mark.gen_test
def test_get_worker(http_client, db_server, worker_base_url):
    # TODO: finish the test
    url = os.path.join(worker_base_url, "2")
    data = yield fetch_url(http_client, url)
    print(data)


@pytest.mark.gen_test
def test_get_all_workers(db_server, worker_base_url):
    # TODO: finish the test
    pass
    # http_client = AsyncHTTPClient()
    # url = worker_base_url
    # data = yield fetch_url(http_client, url)
    # print(data)
