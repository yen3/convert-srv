from datetime import datetime
import os.path

from tornado.httpclient import HTTPRequest
from tornado.escape import json_encode, json_decode
from tornado import gen
import pytest

from test.utils import fetch_url
from common.db.general import session_context
from common.db.task import Task, TaskStatus, FailStatus, get_task
from api_server.api.task import _send_task_channel_event
from api_server.api.task import _send_create_jobs, _send_delete_task
from api_server.api.task import _generate_input_output_path
from api_server.api.task import _remove_input_output_folder_prefix
from api_server.config import REDIS_TASK_CHANNEL
from api_server.daemon.general import TaskEvent, EventHandleDaemon
import api_server.api.task


REDIS_TEST_CHANNEL='qconvert:test'


@pytest.fixture
def task_base_url(base_url):
    return os.path.join(base_url, "api/v1/task")


@pytest.mark.parametrize("post_request,expected",[
    ({'input_path': 'trailer.mp4',
      'output_path': 'trailer_output.mp4'},
     {'input_path': '/mnt/data/trailer.mp4',
      'output_path': '/mnt/data/trailer_output.mp4'}),
    ({'input_path': 'trailer.mp4'},
     {'input_path': '/mnt/data/trailer.mp4',
      'output_path': '/mnt/data/trailer-h264.mp4'}),
    ({'input_path': 'trailer_short.mp4'},
     {'input_path': '/mnt/data/trailer_short.mp4',
      'output_path': '/mnt/data/trailer_short-h264.mp4'}),
])
def test_generate_input_output_path(post_request, expected):
    _generate_input_output_path(post_request)
    assert 'input_path' in post_request
    assert 'output_path' in post_request
    assert post_request == expected


@pytest.mark.parametrize("task,expected",[
    ({'input_path': '/mnt/data/trailer.mp4',
      'output_path': '/mnt/data/trailer_output.mp4',
      'work_dir': '/mnt/data/workspace/task_2', },
     {'input_path': 'trailer.mp4',
      'output_path': 'trailer_output.mp4',
      'work_dir': 'workspace/task_2',},),
    ({'input_path': 'trailer.mp4',
      'output_path': 'trailer_output.mp4',
      'work_dir': 'workspace/task_2',},
     {'input_path': 'trailer.mp4',
      'output_path': 'trailer_output.mp4',
      'work_dir': 'workspace/task_2',},),
])
def test_remove_input_output_folder_prefix(task, expected):
    _remove_input_output_folder_prefix(task)
    assert task == expected


@pytest.mark.gen_test
def test_create_task(http_client, db, task_base_url, monkeypatch):
    monkeypatch.setattr(api_server.api.task, '_send_create_jobs',
                        lambda _: None)

    url = task_base_url
    request_data = {"name": "trailer_convert",
                    "input_path": "trailer.mp4",
                    "output_path": "trailer_output.mp4",
                    "cmd": "ffmpeg",
                    "cmd_options": "-c:v libx264 -c:a copy",
                    "split_unit": 20,
                   }
    request = HTTPRequest(url=url, method="POST",
                          body=json_encode(request_data))
    response = yield http_client.fetch(request)
    assert response.code == 200

    data = json_decode(response.body.decode('utf8'))
    assert data == {'status': 'success',
                    'data': {'create_status': 'success',
                             'id': 2,
                             'msg': 'create task success'}}

    task = get_task(2)
    assert task.id == 2
    assert task.name == 'trailer_convert'
    assert task.cmd == 'ffmpeg'
    assert task.input_path == '/mnt/data/trailer.mp4'
    assert task.output_path == '/mnt/data/trailer_output.mp4'
    assert task.cmd_options == '-c:v libx264 -c:a copy'
    assert task.work_dir == '/mnt/data/workspace/task_2'
    assert task.split_unit == 20
    assert task.status == TaskStatus.creating.value
    assert task.fail_reason == FailStatus.not_run.value
    assert task.timeout == 0
    assert task.create_time <= datetime.now()
    assert task.start_time is None
    assert task.finish_time is None


@pytest.mark.gen_test
def test_create_task_without_output_path(
    http_client, db, task_base_url, monkeypatch):

    monkeypatch.setattr(api_server.api.task, '_send_create_jobs',
                        lambda _: None)

    url = task_base_url
    request_data = {"name": "trailer_convert",
                    "input_path": "trailer.mp4",
                    "cmd": "ffmpeg",
                    "cmd_options": "-c:v libx264 -c:a copy",
                    "split_unit": 20,
                   }
    request = HTTPRequest(url=url, method="POST",
                          body=json_encode(request_data))
    response = yield http_client.fetch(request)
    assert response.code == 200

    data = json_decode(response.body.decode('utf8'))
    assert data == {'status': 'success',
                    'data': {'create_status': 'success',
                             'id': 2,
                             'msg': 'create task success'}}

    task = get_task(2)
    assert task.id == 2
    assert task.name == 'trailer_convert'
    assert task.cmd == 'ffmpeg'
    assert task.input_path == '/mnt/data/trailer.mp4'
    assert task.output_path == '/mnt/data/trailer-h264.mp4'
    assert task.cmd_options == '-c:v libx264 -c:a copy'
    assert task.work_dir == '/mnt/data/workspace/task_2'
    assert task.split_unit == 20
    assert task.status == TaskStatus.creating.value
    assert task.fail_reason == FailStatus.not_run.value
    assert task.timeout == 0
    assert task.create_time <= datetime.now()
    assert task.start_time is None
    assert task.finish_time is None


@pytest.mark.gen_test
def test_create_task_invalid_input_path(http_client,
                                        db, task_base_url, monkeypatch):
    monkeypatch.setattr(api_server.api.task, '_send_create_jobs',
                        lambda _: None)

    url = task_base_url
    request_data = {"name": "trailer_convert",
                    "input_path": "does_not_exist.mp4",
                    "cmd": "ffmpeg",
                    "cmd_options": "-c:v libx264 -c:a copy",
                    "split_unit": 20,
                   }
    request = HTTPRequest(url=url, method="POST",
                          body=json_encode(request_data))
    response = yield http_client.fetch(request)
    assert response.code == 200

    data = json_decode(response.body.decode('utf8'))
    assert data == {'status': 'success',
                    'data': {'create_status': 'fail',
                             'id': -1, 'msg': 'No such input file'}}


@pytest.mark.gen_test
def test_delete_task_fail(http_client, db, task_base_url, monkeypatch, mocker):
    fake_request = None

    def fake_send(request):
        nonlocal fake_request
        fake_request = request

    monkeypatch.setattr(api_server.api.task, '_send_task_channel_event',
                        fake_send)

    url = os.path.join(task_base_url, "42")
    request = HTTPRequest(url=url, method="DELETE")
    response = yield http_client.fetch(request)
    assert response.code == 200
    data = json_decode(response.body.decode('utf8'))

    assert data == {
        'status': 'success',
        'data': {'delete_status': 'fail',
                 'id': 42,
                 'msg': ('can not delete the task, the task is '
                         'not done or fail')}
    }
    assert fake_request is None


@pytest.mark.gen_test
def test_delete_task_succ(http_client, db, task_base_url, monkeypatch, mocker):
    fake_request = None

    def fake_send(request):
        nonlocal fake_request
        fake_request = request

    monkeypatch.setattr(api_server.api.task, '_send_task_channel_event',
                        fake_send)

    # Insert fake job
    with session_context() as session:
        task = Task()
        task.status = TaskStatus.done.value
        session.add(task)
        session.commit()

        task_id = task.id

    url = os.path.join(task_base_url, str(task_id))
    request = HTTPRequest(url=url, method="DELETE")
    response = yield http_client.fetch(request)
    assert response.code == 200
    data = json_decode(response.body.decode('utf8'))
    assert data == {'status': 'success',
                    'data': {'delete_status': 'success', 'id': 2}}
    assert fake_request == {'id': 2,
                            'event': TaskEvent.remove_task.value}


@pytest.mark.gen_test
def test_get_task(http_client, db_server, task_base_url):
    # TODO: finish the test
    url = os.path.join(task_base_url, "2")
    data = yield fetch_url(http_client, url)
    print(data)


@pytest.mark.gen_test
def test_get_all_tasks(http_client, db_server, base_url):
    # TODO: finish the test
    url = os.path.join(base_url, "api/v1/task")
    data = yield fetch_url(http_client, url)
    print(data)


@pytest.mark.gen_test_current
def test_send_task_channel_event():
    @gen.coroutine
    def send_event():
        yield gen.sleep(0.5)
        yield _send_task_channel_event({'id': 2, 'event': 'test_send'})

    def receive_task_event(task_id, request):
        assert task_id == 2
        assert request == {'id': 2, 'event': 'test_send'}

    daemon = EventHandleDaemon(REDIS_TASK_CHANNEL)
    daemon.register(receive_task_event)

    # Workaround for the test
    # subscriber must connect first
    yield daemon._client.connect()

    # Send event
    yield send_event()

    # Receive event
    yield daemon.run(event_max=1)


@pytest.mark.gen_test
def test_send_create_jobs(monkeypatch):
    fake_request = None

    def fake_send(request):
        nonlocal fake_request
        fake_request = request

    monkeypatch.setattr(api_server.api.task, '_send_task_channel_event',
                        fake_send)

    yield _send_create_jobs(2)

    assert fake_request == {'id': 2, 'event': TaskEvent.create_task.value}


@pytest.mark.gen_test
def test_send_delete_task(monkeypatch):
    fake_request = None

    def fake_send(request):
        nonlocal fake_request
        fake_request = request

    monkeypatch.setattr(api_server.api.task, '_send_task_channel_event',
                        fake_send)

    yield _send_delete_task(2)

    assert fake_request == {'id': 2, 'event': TaskEvent.remove_task.value}
