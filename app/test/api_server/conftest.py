import pytest

from common.logging import init_log
from api_server.main import init_app

# init_log()
application = init_app()


@pytest.fixture
def app():
    init_log()
    return application
