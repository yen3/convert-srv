from tornado.escape import json_decode
from tornado import gen


@gen.coroutine
def fetch_url(http_client, url):
    raw_response = yield http_client.fetch(url)
    res = json_decode(raw_response.body.decode('utf8'))
    assert res['status'] == 'success'
    return res['data']
