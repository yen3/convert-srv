from datetime import datetime, timedelta

from tornado.gen import TimeoutError
from tornado import gen
import pytest
import tornado.httpclient
import tornado.testing

from common.db.general import session_context
from common.db.worker import Worker, WorkerStatus, get_worker
from common.redis import RedisClient
from api_server.api.worker_query import fetch_redis_worker_info
from api_server.daemon.worker import REDIS_DATA_WORKER_PREIFX, REDIS_HOST, REDIS_PORT
from api_server.daemon.worker import _get_ping_url
from api_server.daemon.worker import ping_worker
from api_server.daemon.worker import check_worker_alive
from api_server.daemon.worker import work_check_alive_daemon_internal
from api_server.daemon.worker import retrive_unique_worker_info
from api_server.daemon.worker import save_worker_info
from api_server.daemon.worker import collect_worker_status
import common.utils
import api_server.daemon.worker


def test_get_ping_url():
    addr = 'ghost.dev'

    assert _get_ping_url(addr) == "http://ghost.dev:11000/api/v1/"


@pytest.fixture
def test_workers(db):
    fake_timestamp = datetime.strptime('2018-01-31 04:01:08',
                                       '%Y-%m-%d %H:%M:%S')

    workers_data = [
        {"name": "test_ghost1",
         "addr": "ghost1.dev",
         "start_time": fake_timestamp,
         "last_check_time": fake_timestamp,
         "status": "ready"
        },
        {"name": "test_ghost2",
         "addr": "ghost2.dev",
         "start_time": fake_timestamp,
         "last_check_time": fake_timestamp,
         "status": "ready"
        },
        {"name": "test_ghost2",
         "addr": "ghost2.dev",
         "start_time": fake_timestamp,
         "last_check_time": fake_timestamp,
         "status": "ready"
        }
    ]

    with session_context() as session:
        workers = [Worker(**w) for w in workers_data]
        session.add_all(workers)
        session.commit()

    return None


@pytest.mark.slow
@pytest.mark.gen_test(timeout=60)
def test_ping_worker(monkeypatch):
    @gen.coroutine
    def start_worker():
        try:
            yield gen.sleep(1)
            yield common.utils.call("./worker_start.py", [],
                                    cwd="/app", timeout=3)
        except TimeoutError as err:
            pass

    def fake_ping_url(addr, port=None):
        return "http://localhost:11000/api/v1/".format(
            addr=addr)

    monkeypatch.setattr(api_server.daemon.worker,
                        '_get_ping_url', fake_ping_url)

    yield start_worker()

    status = yield ping_worker("ghost.dev")
    assert status is True


@pytest.fixture
def expired_worker(db_server):
    worker_id = 2
    with session_context() as session:
        worker = session.query(Worker).filter(Worker.id==worker_id).one()
        worker.status = WorkerStatus.running.value
        worker.last_check_time = datetime.now() + timedelta(seconds=-1200)

        session.commit()

    return worker_id


@pytest.mark.gen_test
def test_check_worker_alive_not_expired(db_server, monkeypatch):
    @gen.coroutine
    def fake_ping_worker(addr, client=None):
        return True

    monkeypatch.setattr(api_server.daemon.worker,
                        'ping_worker', fake_ping_worker)

    worker_id = 2
    ori_worker = get_worker(worker_id)

    yield check_worker_alive(worker_id)

    worker = get_worker(worker_id)
    assert worker.status == ori_worker.status
    assert worker.last_check_time == ori_worker.last_check_time


@pytest.mark.gen_test
def test_check_worker_alive_expired(expired_worker, monkeypatch):
    @gen.coroutine
    def fake_ping_worker(addr, client=None):
        return True

    monkeypatch.setattr(api_server.daemon.worker,
                        'ping_worker', fake_ping_worker)

    worker_id = expired_worker
    ori_worker = get_worker(worker_id)

    yield check_worker_alive(worker_id)

    worker = get_worker(worker_id)
    assert worker.status == ori_worker.status
    assert worker.last_check_time > ori_worker.last_check_time


@pytest.mark.gen_test
def test_check_worker_alive_expired_and_failed(expired_worker, monkeypatch):
    @gen.coroutine
    def fake_ping_worker(addr, client=None):
        return False

    monkeypatch.setattr(api_server.daemon.worker,
                        'ping_worker', fake_ping_worker)

    worker_id = expired_worker
    ori_worker = get_worker(worker_id)

    yield check_worker_alive(worker_id)

    worker = get_worker(worker_id)
    assert worker.status == WorkerStatus.fail.value
    assert worker.last_check_time > ori_worker.last_check_time


@pytest.mark.slow
@pytest.mark.gen_test
def test_work_check_alive_daemon_internal(expired_worker, monkeypatch):
    @gen.coroutine
    def fake_ping_worker(addr, client=None):
        return True

    monkeypatch.setattr(api_server.daemon.worker,
                        'ping_worker', fake_ping_worker)

    worker_id = expired_worker
    ori_worker = get_worker(worker_id)

    yield work_check_alive_daemon_internal(1)

    worker = get_worker(worker_id)
    assert worker.last_check_time > ori_worker.last_check_time


def test_retrive_unique_worker_info(test_workers):
    workers = retrive_unique_worker_info()

    assert len(workers) == 2
    assert workers == {
        'test_ghost1': {
            'id': 2, 'name': 'test_ghost1', 'addr': 'ghost1.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'},
        'test_ghost2': {
            'id': 4, 'name': 'test_ghost2', 'addr': 'ghost2.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'}}


@pytest.mark.gen_test_current
def test_save_worker_info():
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield client.connect()
    yield client.flushall()

    workers = {
        'test_ghost3': {
            'id': '3', 'name': 'test_ghost3', 'addr': 'ghost3.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'},
        'test_ghost4': {
            'id': '4', 'name': 'test_ghost4', 'addr': 'ghost4.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'}}

    # Save first time
    yield save_worker_info(workers)
    keys = yield client.keys("%s:*" % REDIS_DATA_WORKER_PREIFX)
    keys.sort()

    assert keys == ['qconvert:data:worker:test_ghost3',
                    'qconvert:data:worker:test_ghost4']

    r_workers = yield [client.hgetall(k) for k in keys]
    assert r_workers == list(workers.values())

    yield client.disconnect()


@pytest.mark.gen_test_current
def test_save_worker_info_multiple_times(test_workers):
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield client.connect()
    yield client.flushall()

    prev_workers = {
        'test_ghost2': {
            'id': '2', 'name': 'test_ghost2', 'addr': 'ghost2.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'},
        'test_ghost3': {
            'id': 3, 'name': 'test_ghost3', 'addr': 'ghost3.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'},
        'test_ghost4': {
            'id': 4, 'name': 'test_ghost4', 'addr': 'ghost4.dev',
            'start_time': '2018-01-31 04:01:08',
            'last_check_time': '2018-01-31 04:01:08',
            'status': 'ready'},
    }

    # Save first time
    yield save_worker_info(prev_workers)

    # Save second time
    yield collect_worker_status()

    keys = yield client.keys("%s:*" % REDIS_DATA_WORKER_PREIFX)
    keys.sort()
    assert keys == ['qconvert:data:worker:test_ghost1',
                    'qconvert:data:worker:test_ghost2']

    r_workers = yield fetch_redis_worker_info()
    ans = [{
        'id': '2', 'name': 'test_ghost1', 'addr': 'ghost1.dev',
        'start_time': '2018-01-31 04:01:08',
        'last_check_time': '2018-01-31 04:01:08',
        'status': 'ready'},
        {'id': '4', 'name': 'test_ghost2', 'addr': 'ghost2.dev',
         'start_time': '2018-01-31 04:01:08',
         'last_check_time': '2018-01-31 04:01:08',
         'status': 'ready'}]
    ans.sort(key=lambda x:x['id'])
    r_workers.sort(key=lambda x:x['id'])
    assert r_workers == ans

    yield client.disconnect()
