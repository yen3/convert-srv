from datetime import datetime, timedelta

from tornado import gen
import pytest

from common.db.general import session_context
from common.db.task import Task, TaskStatus
from common.db.job import Job, JobStatus, get_jobs_by_task_id
from api_server.daemon.general import TaskEvent
from api_server.daemon.job import JobEventHandler, JobDaemon
from api_server.daemon.job import detect_task_running
from api_server.daemon.job import detect_task_done
from api_server.daemon.job import detect_task_fail
from api_server.daemon.job import detect_task_timeout_without_worker_stop
from api_server.daemon.job import _fail_job as fail_timeout_job
from api_server.daemon.task import TaskStatus, get_task_status


@pytest.fixture
def fail_timeout_task_id(db_server):
    task_id = 3

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).one()
        task.status = TaskStatus.running.value

        query = session.query(Job).filter(Job.task_id == task_id)
        query = query.order_by(Job.id)
        jobs = query.all()

        session.flush()

        jobs[0].status = JobStatus.running.value
        jobs[0].retry_limit = 2
        jobs[0].retry_time = 1
        jobs[0].start_time = datetime.now() + timedelta(seconds=-300)
        jobs[0].timeout = 60

        jobs[1].status = JobStatus.running.value
        jobs[1].retry_limit = 1
        jobs[1].retry_time = 1
        jobs[1].start_time = datetime.now() + timedelta(seconds=-60)
        jobs[1].timeout = 60

        jobs[2].status = JobStatus.ready.value

        session.commit()

    return task_id


@pytest.mark.gen_test
def test_detect_task_running_none(db_server):
    task_id = 3
    jobs = get_jobs_by_task_id(task_id)

    request = yield detect_task_running(task_id, jobs)
    assert request is None


@pytest.mark.gen_test
def test_detect_task_running(db_server):
    task_id = 3
    jobs = get_jobs_by_task_id(task_id)
    jobs[0].status = JobStatus.running.value

    request = yield detect_task_running(task_id, jobs)
    assert request == {'id': task_id, 'event': TaskEvent.running_task.value}


@pytest.mark.gen_test
def test_detect_task_done_none(db_server):
    task_id = 3
    jobs = get_jobs_by_task_id(task_id)
    jobs[0].status = JobStatus.done.value

    request = yield detect_task_done(task_id, jobs)
    assert request is None


@pytest.mark.gen_test
def test_detect_task_done(db_server):
    task_id = 3
    jobs = get_jobs_by_task_id(task_id)
    for job in jobs:
        job.status = JobStatus.done.value

    request = yield detect_task_done(task_id, jobs)
    assert request == {'id': task_id, 'event': TaskEvent.merge_task.value}


@pytest.mark.gen_test
def test_detect_task_fail_none(db_server):
    task_id = 3
    jobs = get_jobs_by_task_id(task_id)

    request = yield detect_task_fail(task_id, jobs)
    assert request is None


@pytest.mark.gen_test
def test_detect_task_fail(db_server):
    task_id = 3
    jobs = get_jobs_by_task_id(task_id)
    jobs[0].status = JobStatus.fail.value

    request = yield detect_task_fail(task_id, jobs)
    assert request == {'id': task_id, 'event': TaskEvent.fail_task.value}


def test_fail_timeout_job(fail_timeout_task_id):
    task_id = fail_timeout_task_id

    job_ids = [job.id for job in get_jobs_by_task_id(task_id)]

    # Check all jobs
    for job_id in job_ids:
        fail_timeout_job(job_id, JobStatus.running.value)

    jobs = get_jobs_by_task_id(task_id)
    assert jobs[0].status == JobStatus.fail_retry.value
    assert jobs[1].status == JobStatus.fail.value
    assert jobs[2].status == JobStatus.ready.value


@pytest.mark.gen_test
def test_detect_task_timeout_without_worker_stop(fail_timeout_task_id):
    task_id = fail_timeout_task_id
    jobs = get_jobs_by_task_id(task_id)

    yield detect_task_timeout_without_worker_stop(task_id, jobs)

    jobs = get_jobs_by_task_id(task_id)
    assert jobs[0].status == JobStatus.fail_retry.value
    assert jobs[1].status == JobStatus.running.value
    assert jobs[2].status == JobStatus.ready.value


@pytest.mark.gen_test
def test_job_event_handler_notify():
    recv_task_id = None
    recv_request = None

    @gen.coroutine
    def handler(task_id, request):
        nonlocal recv_task_id
        nonlocal recv_request

        recv_task_id = task_id
        recv_request = request

    event_handler = JobEventHandler()
    event_handler.register(handler)
    yield event_handler.notify({"id": 2, "event": TaskEvent.running_task})

    assert recv_task_id == 2
    assert recv_request == {"id": 2, "event": TaskEvent.running_task}


@pytest.mark.gen_test
def test_job_daemon_monitor_and_notify(db_server):
    recv_task_id = None
    recv_request = None

    @gen.coroutine
    def handler(task_id, request):
        nonlocal recv_task_id
        nonlocal recv_request

        recv_task_id = task_id
        recv_request = request

    event_handler = JobEventHandler()
    event_handler.register(handler)

    @gen.coroutine
    def monitor(task_id, job):
        return {"id": task_id, "event": "test_monitor_event"}

    @gen.coroutine
    def monitor2(task_id, job):
        return None

    daemon = JobDaemon(event_handler)
    daemon.register(monitor)

    task_id = 3
    jobs = get_jobs_by_task_id(task_id)

    rtv = yield daemon._monitor(task_id, jobs)

    assert rtv is True
    assert recv_task_id == task_id
    assert recv_request == {"id": task_id, "event": "test_monitor_event"}


@pytest.mark.gen_test
@pytest.mark.slow
def test_job_daemon_run(db_server):
    recv_task_ids = []
    recv_requests = []

    @gen.coroutine
    def handler(task_id, request):
        nonlocal recv_task_ids
        nonlocal recv_requests

        recv_task_ids.append(task_id)
        recv_requests.append(request)

    event_handler = JobEventHandler()
    event_handler.register(handler)

    @gen.coroutine
    def monitor(task_id, job):
        return {"id": task_id, "event": "test_monitor_event"}

    @gen.coroutine
    def monitor2(task_id, job):
        return None

    daemon = JobDaemon(event_handler, sleep_secs=1)
    daemon.register(monitor)

    task_id = 3
    jobs = get_jobs_by_task_id(task_id)

    yield daemon.run(event_max=1)

    assert recv_task_ids == [2, 3]
    assert recv_requests == [{'id': 2, 'event': 'test_monitor_event'},
                             {'id': 3, 'event': 'test_monitor_event'}]
