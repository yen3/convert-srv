import pytest

from common.db.task import TaskStatus
from common.db.task import change_task_status, get_task, get_task_status
from common.db.job import JobStatus, FailStatus, get_jobs_by_task_id
from api_server.daemon.general import TaskEvent
from api_server.daemon.task import init_task as daemon_init_task
from api_server.daemon.task import merge_task_output as daemon_merge_task_output
from api_server.daemon.task import set_task_running
from api_server.daemon.task import fail_task
from api_server.daemon.task import clean_finished_task
import api_server.daemon.task
import common.db.task


@pytest.fixture
def fake_done_task(db_server):
    task_id = 3

    change_task_status(task_id, TaskStatus.done.value)

    yield task_id


@pytest.mark.gen_test
def test_init_task(db_server, monkeypatch):
    init_task_call = False

    def fake_init_task(task):
        nonlocal init_task_call
        init_task_call = True

        return True

    fake_task_init_func = {"ffmpeg": fake_init_task}

    monkeypatch.setattr(api_server.daemon.task, 'task_init_func',
                        fake_task_init_func)

    yield daemon_init_task(2, {'id': 2, 'event': 'create_task'})

    assert init_task_call is True

    task = get_task(2)
    assert task.status == TaskStatus.ready.value


@pytest.mark.gen_test
def test_merge_task_output(db_server, monkeypatch):
    merge_task_call = False

    def fake_merge_task(task):
        nonlocal merge_task_call
        merge_task_call = True

        return True

    fake_task_merge_func = {"ffmpeg": fake_merge_task}

    monkeypatch.setattr(api_server.daemon.task, 'task_merge_func',
                        fake_task_merge_func)

    yield daemon_merge_task_output(2, {'id': 2, 'event': 'merge_task'})

    assert merge_task_call is True

    task = get_task(2)
    assert task.status == TaskStatus.done.value


@pytest.mark.gen_test
def test_set_task_running_none(db_server):
    task_id = 3
    origin_task_status = get_task_status(task_id)
    request = {'id': task_id, 'event': "Not running"}

    yield set_task_running(task_id, request)

    assert get_task_status(task_id) == origin_task_status


@pytest.mark.gen_test
def test_set_task_running(db_server):
    task_id = 3
    request = {'id': task_id, 'event': TaskEvent.running_task.value}

    yield set_task_running(task_id, request)

    assert get_task_status(task_id) == TaskStatus.running.value


@pytest.mark.gen_test
def test_fail_task_none(db_server):
    task_id = 3
    origin_task_status = get_task_status(task_id)
    request = {'id': task_id, 'event': "Not fail"}

    yield fail_task(task_id, request)

    assert get_task_status(task_id) == origin_task_status


@pytest.mark.gen_test
def test_fail_task_none(db_server):
    task_id = 3
    request = {'id': task_id, 'event': TaskEvent.fail_task.value}

    yield fail_task(task_id, request)

    jobs = get_jobs_by_task_id(task_id)
    for job in jobs:
        assert job.status == JobStatus.fail.value
        assert job.fail_reason == FailStatus.cancel.value


@pytest.mark.gen_test
def test_clean_finished_task_none(fake_done_task, mocker):
    mocker.spy(common.db.task, 'delete_task')

    task_id = 42

    yield clean_finished_task(task_id,
                              {'id': task_id,
                               'event': TaskEvent.remove_task.value})

    assert common.db.task.delete_task.call_count == 0


@pytest.mark.gen_test
def test_clean_finished_task(fake_done_task, mocker):
    mocker.spy(common.db.task, 'delete_task')
    task_id = fake_done_task

    yield clean_finished_task(task_id,
                              {'id': task_id,
                               'event': TaskEvent.remove_task.value})

    task = get_task(task_id)
    jobs = get_jobs_by_task_id(task_id)

    assert task is None
    assert not jobs
