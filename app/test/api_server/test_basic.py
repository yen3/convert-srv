import os.path

from tornado.escape import json_decode
import pytest


@pytest.mark.gen_test
def test_server_ping(http_client, base_url):
    fetch_url = os.path.join(base_url, "api/v1/")
    response = yield http_client.fetch(fetch_url)

    assert response.code == 200

    msg = json_decode(response.body.decode('utf8'))
    assert msg == {'status': 'success',
                   'data': 'video-convert is running'}


