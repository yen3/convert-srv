import os
import os.path

import pytest

from common.utils import call
from common.utils import utf8_str
from common.utils import dict_str_b2s
from common.utils import file_suffix
from common.utils import path_without_suffix


@pytest.yield_fixture
def output_path():
    path = "./hello.txt"

    try:
        os.remove(path)
    except OSError:
        pass

    yield path

    try:
        os.remove(path)
    except OSError:
        pass


@pytest.mark.gen_test
def test_call():
    cmd = "echo"
    options = ["hello", "world"]
    proc = yield call(cmd, options)

    assert proc['return_code'] == 0
    assert proc['timeout'] is False
    assert proc['stdout'] == "hello world\n"
    assert proc['stderr'] == ""


@pytest.mark.gen_test
def test_call_timeout():
    cmd = "echo"
    options = ["hello", "world"]
    proc = yield call(cmd, options, timeout=10)

    assert proc['return_code'] == 0
    assert proc['timeout'] is False
    assert proc['stdout'] == "hello world\n"
    assert proc['stderr'] == ""


@pytest.mark.slow
@pytest.mark.gen_test
def test_call_with_timeout_handle():
    cmd = "sleep"
    options = ["3"]
    proc = yield call(cmd, options, timeout=1)

    assert proc['return_code'] == -1
    assert proc['timeout'] is True
    assert proc['stdout'] == ""
    assert proc['stderr'] == ""


@pytest.mark.gen_test
def test_basic_no_output(output_path):
    cmd = "./test/common/test_write_file.sh"
    options = [output_path]
    proc = yield call(cmd, options, redirect=False)

    assert proc['return_code'] == 0
    assert proc['timeout'] is False
    assert proc['stdout'] == ""
    assert proc['stderr'] == ""

    assert os.path.isfile(output_path)
    with open(output_path) as fin:
        s = fin.read()
        assert s == "hello world\n"


@pytest.mark.gen_test
def test_basic_no_output_timeout(output_path):
    cmd = "./test/common/test_write_file.sh"
    options = [output_path]
    proc = yield call(cmd, options, redirect=False, timeout=100)

    assert proc['return_code'] == 0
    assert proc['timeout'] is False
    assert proc['stdout'] == ""
    assert proc['stderr'] == ""

    assert os.path.isfile(output_path)
    with open(output_path) as fin:
        s = fin.read()
        assert s == "hello world\n"


@pytest.mark.slow
@pytest.mark.gen_test
def test_with_timeout_handle_no_output():
    cmd = "sleep"
    options = ["3"]
    proc = yield call(cmd, options, timeout=1, redirect=False)

    assert proc['return_code'] == -1
    assert proc['timeout'] is True
    assert proc['stdout'] == ""
    assert proc['stderr'] == ""


@pytest.mark.parametrize("ori_str,expected", [
    ('123', '123'),
    (b'bytes', 'bytes'),
    (1, '1'),
])
def test_utf8_str(ori_str, expected):
    assert utf8_str(ori_str) == expected


def test_ut8_str_exception():
    s = 1
    with pytest.raises(NotImplementedError,
                       message="Can not convert: " + str(1)):
        utf8_str(s, force=False)


def test_dict_str_b2s():
    d = {b'a':  'a',
         b'b': b'b',
          'c':  'c',
          'd': b'd'}

    ans_d = {'a': 'a', 'b': 'b', 'c': 'c', 'd': 'd'}
    assert dict_str_b2s(d) == ans_d


@pytest.mark.parametrize("path,suffix",[
    ("test.mp4", ".mp4"),
    ("/mnt/data/test.mov", ".mov"),
    ("test", ""),
    ("/mnt/data/test", ""),
])
def test_file_suffix(path, suffix):
    assert file_suffix(path) == suffix


@pytest.mark.parametrize("path,expected",[
    ("", ""),
    ("test.mp4", "test"),
    ("/mnt/data/test.mov", "/mnt/data/test"),
    ("test", "test"),
    ("/mnt/data/test", "/mnt/data/test"),
])
def test_path_without_suffix(path, expected):
    assert path_without_suffix(path) == expected
