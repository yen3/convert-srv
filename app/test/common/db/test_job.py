from datetime import datetime

import pytest

from test.utils import datetime_equal
from common.db.general import session_context
from common.db.job import Job, JobLog, JobStatus
from common.db.job import retrieve_new_job, get_job
from common.db.job import upload_new_job_log
from common.db.job import job_dict


def test_retrieve_new_job_empty_db(db):
    worker_id = 2

    job_id = retrieve_new_job(worker_id)
    assert job_id is None


def test_retrieve_new_job(db_worker):
    worker_id = 2

    job_id = retrieve_new_job(worker_id)
    assert job_id is not None

    job = get_job(job_id)

    assert job.worker_id == worker_id
    assert job.status == JobStatus.running.value
    assert job.retry_time == 1
    assert datetime_equal(job.start_time, datetime.now(), comp_second=False)


def test_retrieve_new_job_all_invalid(db):
    # Write test data
    with session_context() as session:
        job1 = Job(status=JobStatus.running.value)
        job2 = Job(status=JobStatus.fail.value)
        job3 = Job(status=JobStatus.done.value)

        session.add_all([job1, job2, job3])

    worker_id = 2
    job_id = retrieve_new_job(worker_id)
    assert job_id is None


def test_upload_new_job_log(db):
    insert_job_log = JobLog(job_id=2,
                            worker_id=2,
                            retry_time=1,
                            return_code=0,
                            cmd="echo",
                            cmd_options="hello world",
                            stdout_log="hello world\n",
                            stderr_log="",
                            start_time=datetime.now(),
                            finish_time=datetime.now())

    log_id = upload_new_job_log(insert_job_log)
    assert log_id is not None

    with session_context() as session:
        job_log = session.query(JobLog).filter(JobLog.id == log_id).one()

        session.flush()
        session.expunge(job_log)

    assert job_log.job_id == 2
    assert job_log.worker_id == 2
    assert job_log.retry_time == 1
    assert job_log.return_code == 0
    assert job_log.cmd == "echo"
    assert job_log.cmd_options == "hello world"
    assert job_log.stdout_log == "hello world\n"
    assert job_log.stderr_log == ""


def test_job_dict(db_worker):
    job = get_job(2)

    jd = job_dict(job)
    print(jd)
