from datetime import datetime, timedelta

import pytest

from common.db.general import session_context
from common.db.task import Task, TaskStatus
from common.db.task import get_all_task_ids, get_task_status, get_task
from common.db.task import change_task_status
from common.db.task import delete_task
from common.db.task import refresh_task_start_time
from common.db.job import Job, get_jobs_by_task_id
from test.utils import datetime_now


def test_get_all_task_ids_empty(db):
    task_ids = get_all_task_ids()
    assert task_ids == []


def test_get_all_task_ids(db_server):
    task_ids = get_all_task_ids()
    assert task_ids == [2, 3]


@pytest.mark.parametrize("task_id,expected",[
    (42, None),
    (2, TaskStatus.creating.value),
])
def test_get_task_status(task_id, expected, db_server):
    assert get_task_status(task_id) == expected


@pytest.mark.parametrize("status",[
    (TaskStatus.done.value),
    (TaskStatus.fail.value)
])
def test_change_task_status(status, db_server):
    task_id = 2
    change_task_status(task_id, status)
    assert get_task_status(task_id) == status


def test_delete_task_db_empty(db_server):
    task_id = 99
    rtv = delete_task(task_id)
    assert rtv is True


def test_delete_task_db(db_server):
    task_id = 3

    task = get_task(task_id)
    jobs = get_jobs_by_task_id(task_id)

    assert task is not None
    assert jobs

    rtv = delete_task(task_id)
    assert rtv is True

    task = get_task(task_id)
    jobs = get_jobs_by_task_id(task_id)

    assert task is None
    assert not jobs


def test_refresh_task_start_time(db):
    # Insert test data
    with session_context() as session:
        task = Task()
        session.add(task)
        session.commit()
        task_id = task.id

        job = Job()
        job.task_id = task_id
        session.add(job)
        session.commit()
        job_id = job.id

    # Check pre-condition
    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).one()
        assert task.start_time is None

    refresh_task_start_time(job_id)

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).one()
        assert isinstance(task.start_time, datetime)
        assert task.start_time <= datetime.now()


def test_refresh_task_start_time_invalid(db):
    refresh_task_start_time(42)

    with session_context() as session:
        task = session.query(Task).filter(Task.id == 42).first()
        assert task is None

def test_refresh_task_start_time_already_set(db):
    fake_start_time = datetime_now() + timedelta(seconds=-60)

    # Insert test data
    with session_context() as session:
        task = Task()
        task.start_time = fake_start_time
        session.add(task)
        session.commit()
        task_id = task.id

        job = Job()
        job.task_id = task_id
        session.add(job)
        session.commit()
        job_id = job.id

    refresh_task_start_time(job_id)

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).one()
        assert task.start_time == fake_start_time
