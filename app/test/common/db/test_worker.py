from datetime import datetime

import pytest

from test.utils import datetime_now
from common.db.general import session_context
from common.db.worker import Worker, WorkerStatus
from common.db.worker import register_current_worker
from common.db.worker import unregister_worker
from common.db.worker import renew_worker_check_time
from common.db.worker import get_worker
from common.db.worker import get_all_worker_ids


def test_register_current_worker(db_worker):
    name = "test_name"
    addr = "test_addr"
    start_time = datetime_now()

    new_id = register_current_worker(name, addr, start_time=start_time)

    with session_context() as session:
        w = session.query(Worker).filter(Worker.name == "test_name").one()
        session.expunge(w)

    assert new_id == w.id
    assert w.name == name
    assert w.addr == addr
    assert w.start_time == start_time
    assert w.status == WorkerStatus.ready.value


def test_register_current_worker_no_argument(db_worker):
    new_id = register_current_worker()

    with session_context() as session:
        w = session.query(Worker.id, Worker.name, Worker.addr).order_by(
            Worker.id.desc()).first()

    assert new_id == w.id
    assert w.name != 'test_ghost'
    assert w.addr != 'ghost.dev'


def test_unregister_worker(db_worker):
    with session_context() as session:
        worker_id = session.query(
            Worker.id.label('worker_id')).first().worker_id

    unregister_worker(worker_id)

    with session_context() as session:
        worker = session.query(Worker).filter(Worker.id == worker_id).one()
        session.expunge(worker)

    assert worker.id == worker_id
    assert worker.status == WorkerStatus.offline.value


def test_renew_worker_check_time(db_worker):
    worker_id = None

    with session_context() as session:
        worker = session.query(Worker).first()
        worker_id = worker.id
        worker.last_check_time = datetime(1970, 1, 1)

    curr_time = datetime_now()
    renew_worker_check_time(worker_id, curr_time)

    with session_context() as session:
        last_check_time = session.query(
            Worker.last_check_time.label('last_check_time')).filter(
            Worker.id == worker_id).first().last_check_time

        assert last_check_time == curr_time


def test_renew_worker_check_time_no_argument(db_worker):
    worker_id = None
    curr_time = datetime_now()

    with session_context() as session:
        worker = session.query(Worker).first()
        worker_id = worker.id
        worker.last_check_time = datetime(1970, 1, 1)

    renew_worker_check_time(worker_id)

    with session_context() as session:
        last_check_time = session.query(
            Worker.last_check_time.label('last_check_time')).filter(
            Worker.id == worker_id).first().last_check_time

        assert last_check_time >= curr_time


def test_get_all_worker_ids(db_worker):
    worker_ids = get_all_worker_ids()
    assert worker_ids == [2]


def test_get_worker(db_server):
    worker_id = 2

    worker = get_worker(worker_id)

    assert worker is not None
    assert worker.name == "test_ghost"
    assert worker.addr == "ghost.dev"
    assert worker.start_time is not None
