from tornado import gen
import pytest
import tornadis

from common.redis import RedisClient
from common.redis import RedisPublisher, RedisSubscriptor
from common.redis import RedisClientConextManager


REDIS_HOST='redis'
REDIS_PORT=6379
REDIS_TEST_CHANNEL='qconvert:test'
REDIS_TEST_DATA_PREFIX='qconvert:testdata:'


@pytest.mark.gen_test_current
def test_client_ping():
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield client.connect()

    msg = yield client.ping()
    assert msg == "PONG"

    yield client.disconnect()


@pytest.mark.gen_test_current
def test_client_ping_context_manager():
    with RedisClientConextManager(host=REDIS_HOST, port=REDIS_PORT,
                                  autoconnect=True)  as client:

        msg = yield client.ping()
        assert msg == "PONG"


@pytest.mark.gen_test_current
def test_client_pub_sub():
    @gen.coroutine
    def send_msg(client):
        yield gen.sleep(0.5)
        client.publish('Hello World1')

    # Publisher
    pub = RedisPublisher(REDIS_TEST_CHANNEL,
                         True, host=REDIS_HOST, port=REDIS_PORT)
    yield pub.connect()

    # Subscriptor
    sub = RedisSubscriptor(REDIS_TEST_CHANNEL,
                           host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield sub.connect()

    # Send message
    yield send_msg(pub)

    # Receive message
    msg = yield sub.subscribe()
    assert msg == {'type': 'message',
                   'channel': REDIS_TEST_CHANNEL,
                   'message': 'Hello World1'}

    yield sub.disconnect()
    yield pub.disconnect()


@pytest.mark.gen_test_current
def test_client_pub_sub_json_message():
    data = {'id': 1, 'msg': 'hello world'}

    @gen.coroutine
    def send_msg(client):
        yield gen.sleep(0.5)
        client.publish_json(data)

    # Publisher
    pub = RedisPublisher(REDIS_TEST_CHANNEL,
                         True, host=REDIS_HOST, port=REDIS_PORT)
    yield pub.connect()

    # Subscriptor
    sub = RedisSubscriptor(REDIS_TEST_CHANNEL,
                           host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield sub.connect()

    # Send message
    yield send_msg(pub)

    # Receive message
    msg = yield sub.subscribe_json()
    assert msg == {'type': 'message',
                   'channel': REDIS_TEST_CHANNEL,
                   'message': data}

    yield sub.disconnect()
    yield pub.disconnect()


@pytest.mark.gen_test_current
def test_client_pub_sub_multi():
    @gen.coroutine
    def send_msg(client):
        yield gen.sleep(0.5)
        client.publish('Hello World1')
        client.publish('Hello World2')
        client.publish('Hello World3')
        client.publish('Hello World4')

    # Publisher
    pub = RedisPublisher(REDIS_TEST_CHANNEL,
                         host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield pub.connect()

    # Subscriptor
    sub = RedisSubscriptor(REDIS_TEST_CHANNEL,
                           host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield sub.connect()

    # Send message
    yield send_msg(pub)

    # Receive message
    msgs = yield [sub.subscribe(), sub.subscribe(),
                  sub.subscribe(), sub.subscribe()]
    msgs = [m['message'] for m in msgs]
    assert msgs == ['Hello World1', 'Hello World2',
                    'Hello World3', 'Hello World4',]

    # TODO: think how to test the case
    # error = yield sub.subscribe()
    # assert isinstance(error, tornadis.TornadisException)

    yield sub.disconnect()
    yield pub.disconnect()


@pytest.mark.gen_test_current
@pytest.mark.parametrize("data,expected",[
    ("hello", "hello"),
    (2, "2"),
])
def test_redis_client_set_key_del(data, expected):
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield client.connect()
    yield client.flushall()

    key = REDIS_TEST_DATA_PREFIX + ":" + "test"

    rtv = yield client.hset(key, "name", data)
    assert rtv == 1

    msg = yield client.hget(key, "name")
    assert msg == expected

    rtv = yield client.delete(key)
    assert rtv == 1

    yield client.disconnect()


@pytest.mark.gen_test_current
def test_redis_client_keys():
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield client.connect()
    yield client.flushall()

    key1 = REDIS_TEST_DATA_PREFIX + ":" + "test1"
    key2 = REDIS_TEST_DATA_PREFIX + ":" + "test2"

    yield client.hset(key1, "name", "hello")
    yield client.hset(key2, "name", "world")

    keys = yield client.keys(REDIS_TEST_DATA_PREFIX + ":*")
    assert key1 in keys
    assert key2 in keys

    yield client.disconnect()


@pytest.mark.gen_test_current
def test_redis_client_hmset_hgetall():
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    yield client.connect()
    yield client.flushall()

    data = {"name": "worker1", "addr": "ghost.dev"}
    key = REDIS_TEST_DATA_PREFIX + ":" + "test1"

    rtv = yield client.hmset(key, data)
    assert rtv == 'OK'

    fetch_data = yield client.hgetall(key)
    assert fetch_data == data

    yield client.disconnect()
