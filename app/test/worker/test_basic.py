import os.path

from tornado.escape import json_decode
from tornado.httputil import url_concat
import pytest
import worker.config


@pytest.fixture
def ping_url(base_url):
    return os.path.join(base_url, "api/v1/")


@pytest.mark.gen_test
def test_ping(http_client, ping_url):
    url = ping_url
    response = yield http_client.fetch(url)

    assert response.code == 200

    msg = json_decode(response.body.decode('utf8'))
    assert msg == {'status': 'success',
                   'data': 'worker is running'}


def test_work_id_patch(monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 2)

    wi = worker.config.WorkerInfo()
    assert wi.id == 2
