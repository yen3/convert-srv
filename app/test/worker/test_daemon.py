from datetime import datetime
import shutil

import pytest
import tornado.gen

from test.utils import datetime_now, datetime_equal
from common.db.general import session_context
from common.db.task import get_task
from common.db.job import get_job, Job, JobLog, JobStatus, FailStatus
from common.db.worker import Worker, WorkerStatus
from worker.daemon import retrieve_new_job
from worker.daemon import execute_cmd
from worker.daemon import upload_log
from worker.daemon import update_job_status
from worker.daemon import run_job
from worker.daemon import set_worker_running
from worker.daemon import replace_cmd_argumets
import worker.config
import common.db.worker


@pytest.yield_fixture
def task_work_folder():
    yield None

    try:
        shutil.rmtree('/mnt/data/workspace/task_2')
    except:
        pass


@pytest.yield_fixture
def worker_id():
    worker_id = common.db.worker.register_current_worker(name="pytest",
                                                         addr="pytest")

    yield worker_id

    common.db.worker.unregister_worker(worker_id)


@pytest.mark.parametrize("s, formatted_s", [
    ('', ''),
    ('{{worker_id}}', '42'),
    ('temp_output_01_{{worker_id}}.mp4', 'temp_output_01_42.mp4'),
    ('temp_output_01.mp4', 'temp_output_01.mp4')
])
def test_replace_cmd_arguments(s, formatted_s, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    new_s = replace_cmd_argumets(s)
    assert new_s == formatted_s


@pytest.mark.gen_test
def test_set_worker_running(worker_id, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', worker_id)

    def make_query(session):
        query = session.query(Worker.status.label('status'))
        query = query.filter(Worker.id == worker_id)
        return query

    def get_current_status():
        status = None
        with session_context() as session:
            status = make_query(session).one().status
            session.flush()

            session.commit()

        return status

    assert get_current_status() == WorkerStatus.ready.value

    with set_worker_running():
        assert get_current_status() == WorkerStatus.running.value

    assert get_current_status() == WorkerStatus.ready.value


@pytest.mark.gen_test
def test_retrieve_new_job_empty_db(db):
    job_id = yield retrieve_new_job()

    assert job_id is None


@pytest.mark.gen_test
def test_retrieve_new_job(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job_id = yield retrieve_new_job()

    assert job_id is not None

    job = get_job(job_id)

    assert job.id == 2
    assert job.worker_id == 42
    assert job.status == JobStatus.running.value
    assert job.retry_time == 1
    assert job.retry_time <= job.retry_limit


@pytest.mark.gen_test
def test_retrieve_new_job_many_time(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job_ids = yield [retrieve_new_job(), retrieve_new_job(),
                     retrieve_new_job(), retrieve_new_job(),
                     retrieve_new_job()]
    assert job_ids == [2, 3, 4, 5, 6]

    worker_ids = [get_job(job_id).worker_id for job_id in job_ids]
    assert set(worker_ids) == {42}

    none_job_id = yield retrieve_new_job()
    assert none_job_id is None


@pytest.mark.gen_test
def test_execute_cmd(db_worker, task_work_folder, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job = get_job(2)

    result = yield execute_cmd(job)
    assert result is not None

    now = datetime.now()

    assert result['return_code'] == 0
    assert result['stdout'] == job.work_dir + "\n"
    assert result['stderr'] == ""
    assert result['timeout'] is False
    assert result['error'] == ""
    assert datetime_equal(result['start_time'], now)
    assert datetime_equal(result['start_time'], result['finish_time'])



@pytest.mark.slow
@pytest.mark.gen_test
def test_execute_cmd_timeout(db_worker, task_work_folder, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job = get_job(4)

    result = yield execute_cmd(job)
    assert result is not None

    assert result['return_code'] == -1
    assert result['stdout'] == ""
    assert result['stderr'] == ""
    assert result['timeout'] is True
    assert result['error'] == "process timeout"


@pytest.mark.gen_test
def test_execute_cmd_fail(db_worker, task_work_folder, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job = get_job(5)

    now = datetime.now()

    result = yield execute_cmd(job)
    assert result is not None
    assert result['return_code'] == -1
    assert result['stdout'] == ""
    assert result['stderr'] == ""
    assert result['timeout'] is False
    assert "FileNotFoundError" in result['error']
    assert datetime_equal(result['start_time'], now)
    assert datetime_equal(result['start_time'], result['finish_time'])


@pytest.mark.gen_test
def test_execute_cmd_output_cmd_options(db_worker, task_work_folder,
                                        monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job = get_job(6)

    result = yield execute_cmd(job)

    assert result is not None
    assert result['return_code'] == 0
    assert result['cmd_options'] == 'output_02_42.txt'
    assert result['stdout'] == 'output_02_42.txt\n'
    assert result['output_path'] == \
        '/mnt/data/workspace/task_2/output_02_42.txt'
    assert result['timeout'] is False
    assert result['error'] == ''


@pytest.mark.gen_test
def test_upload_log(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    # Run the job and upload new job load
    job = get_job(2)
    result = yield execute_cmd(job)
    log_id = yield upload_log(job, result)

    # Get the job log
    with session_context() as session:
        log = session.query(JobLog).filter(JobLog.id == log_id).one()
        session.flush()

        session.expunge(log)
        session.commit()

    assert log.job_id == job.id
    assert log.task_id == job.task_id
    assert log.worker_id == 42
    assert log.retry_time == 0
    assert log.return_code == 0
    assert log.error_msg == result['error']
    assert log.is_timeout == result['timeout']
    assert log.cmd == job.cmd
    assert log.cmd_options == job.cmd_options
    assert log.work_dir == job.work_dir
    assert log.stdout_log == result['stdout']
    assert log.stderr_log == result['stderr']
    assert log.start_time == result['start_time']
    assert log.finish_time == result['finish_time']


@pytest.mark.slow
@pytest.mark.gen_test
def test_upload_log_timeout(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    # Run the job and upload new job load
    job = get_job(4)
    result = yield execute_cmd(job)
    log_id = yield upload_log(job, result)

    # Get the job log
    with session_context() as session:
        log = session.query(JobLog).filter(JobLog.id == log_id).one()
        session.flush()

        session.expunge(log)
        session.commit()

    assert log.job_id == job.id
    assert log.task_id == job.task_id
    assert log.worker_id == 42
    assert log.retry_time == 0
    assert log.return_code == -1
    assert log.error_msg == result['error']
    assert log.is_timeout is True
    assert log.is_timeout == result['timeout']
    assert log.cmd == job.cmd
    assert log.cmd_options == job.cmd_options
    assert log.work_dir == job.work_dir
    assert log.stdout_log == result['stdout']
    assert log.stderr_log == result['stderr']
    assert log.start_time == result['start_time']
    assert log.finish_time == result['finish_time']


@pytest.mark.gen_test
def test_upload_log_fail(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    # Run the job and upload new job load
    job = get_job(5)
    result = yield execute_cmd(job)
    log_id = yield upload_log(job, result)

    # Get the job log
    with session_context() as session:
        log = session.query(JobLog).filter(JobLog.id == log_id).one()
        session.flush()

        session.expunge(log)
        session.commit()

    assert log.job_id == job.id
    assert log.task_id == job.task_id
    assert log.worker_id == 42
    assert log.retry_time == 0
    assert log.return_code == -1
    assert 'FileNotFoundError' in log.error_msg
    assert log.error_msg == result['error']
    assert log.is_timeout is False
    assert log.is_timeout == result['timeout']
    assert log.cmd == job.cmd
    assert log.cmd_options == job.cmd_options
    assert log.work_dir == job.work_dir
    assert log.stdout_log == result['stdout']
    assert log.stderr_log == result['stderr']
    assert log.start_time == result['start_time']
    assert log.finish_time == result['finish_time']


@pytest.mark.gen_test
def test_upload_log_output_path_cmd_options(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    job = get_job(6)
    result = yield execute_cmd(job)
    log_id = yield upload_log(job, result)

    # Get the job log
    with session_context() as session:
        log = session.query(JobLog).filter(JobLog.id == log_id).one()
        session.flush()

        session.expunge(log)
        session.commit()

    assert job is not None
    assert log.job_id == job.id
    assert log.task_id == job.task_id
    assert log.worker_id == 42
    assert log.retry_time == 0
    assert log.return_code == 0
    assert log.error_msg == ''
    assert log.error_msg == result['error']
    assert log.is_timeout is False
    assert log.is_timeout == result['timeout']
    assert log.cmd == job.cmd
    assert log.cmd_options != job.cmd_options
    assert log.cmd_options == 'output_02_42.txt'
    assert log.work_dir == job.work_dir
    assert log.stdout_log == 'output_02_42.txt\n'
    assert log.stdout_log == result['stdout']
    assert log.stderr_log == result['stderr']
    assert log.start_time == result['start_time']
    assert log.finish_time == result['finish_time']


@pytest.mark.gen_test
def test_update_job_status(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    now = datetime_now()
    result = {'return_code': 0,
              'output_path': '',
              'cmd_options': '',
              'stdout': "/mnt/test_does_not_exist",
              'stderr': "",
              'timeout': False,
              'error': '',
              'start_time': now,
              'finish_time': now}

    # retrieve job
    job_id = yield retrieve_new_job()
    assert job_id is not None

    # Update
    ori_job = get_job(job_id)
    rtv = yield update_job_status(ori_job, result)
    assert rtv is True
    assert ori_job.finish_time is None

    # Verify
    job = get_job(job_id)
    assert job.worker_id == 42
    assert job.retry_time == 1
    assert job.status == JobStatus.done.value
    assert job.fail_reason == FailStatus.done.value
    assert job.start_time is not None
    assert job.finish_time == now


@pytest.mark.gen_test
def test_update_job_status_fail_retry(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    now = datetime_now()
    result = {'return_code': -1,
              'output_path': '',
              'cmd_options': 'output_02_42.txt',
              'stdout': "output_02_42.txt\n",
              'stderr': "",
              'timeout': False,
              'error': '',
              'start_time': now,
              'finish_time': now}

    # retrieve job
    job_id = yield retrieve_new_job()
    assert job_id is not None

    # Update
    ori_job = get_job(job_id)
    rtv = yield update_job_status(ori_job, result)
    assert rtv is True

    # Verify
    job = get_job(job_id)
    assert job.status == JobStatus.fail_retry.value
    assert job.fail_reason == FailStatus.fatal.value
    assert job.output_path == ori_job.output_path
    assert job.cmd_options == ori_job.cmd_options
    assert job.retry_time == 1

    # get job again
    new_job_id = yield retrieve_new_job()
    assert new_job_id == job_id

    # Update
    ori_job = get_job(new_job_id)
    rtv = yield update_job_status(ori_job, result)
    assert rtv is True

    job = get_job(job_id)
    assert job.status == JobStatus.fail.value
    assert job.fail_reason == FailStatus.fatal.value
    assert job.retry_time == 2


@pytest.mark.gen_test
def test_update_job_status_timeout(db_worker, monkeypatch):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    now = datetime_now()
    result = {'return_code': -1,
              'output_path': '',
              'cmd_options': '',
              'stdout': "",
              'stderr': "",
              'timeout': True,
              'error': 'process timeout',
              'start_time': now,
              'finish_time': now}

    # retrieve job
    job_id = yield retrieve_new_job()
    assert job_id is not None
    assert job_id == 2

    # Update
    ori_job = get_job(job_id)
    rtv = yield update_job_status(ori_job, result)
    assert rtv is True
    assert ori_job.finish_time is None

    # Verify
    job = get_job(job_id)
    assert job.worker_id == 42
    assert job.status == JobStatus.fail_retry.value
    assert job.fail_reason == FailStatus.timeout.value
    assert job.output_path == ori_job.output_path
    assert job.cmd_options == ori_job.cmd_options


@pytest.mark.slow
@pytest.mark.gen_test
def test_run_job_empty_db(db, task_work_folder, monkeypatch, mocker):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', 42)

    mocker.spy(tornado.gen, 'sleep')

    yield run_job()

    assert tornado.gen.sleep.call_count == 1


@pytest.mark.gen_test
def test_run_job_normal(db_worker, worker_id, task_work_folder,
                        monkeypatch, mocker):
    monkeypatch.setattr(worker.config.WorkerInfo, 'id', worker_id)

    mocker.spy(worker.daemon, 'set_worker_running')

    yield run_job()

    assert worker.daemon.set_worker_running.call_count == 1

    job = get_job(2)
    assert job.worker_id == worker_id
    assert job.status == JobStatus.done.value
    assert job.fail_reason == FailStatus.done.value
    assert job.retry_time == 1

    task = get_task(job.task_id)
    assert task.create_time <= task.start_time
    assert task.start_time <= datetime.now()

    with session_context() as session:
        logs = session.query(JobLog).filter(JobLog.job_id == job.id).all()
        session.flush()
        assert len(logs) == 1

        job_log = logs[0]
        assert job_log.job_id == job.id
        assert job_log.task_id == job.task_id
        assert job_log.worker_id == worker_id
        assert job_log.stdout_log == job.work_dir + "\n"
        assert job_log.stderr_log == ""
        assert job_log.error_msg == ""
        assert job_log.is_timeout is False
        assert job_log.cmd == job.cmd
        assert job_log.cmd_options == job.cmd_options
        assert job_log.work_dir == job.work_dir
        assert job_log.start_time is not None
        assert job_log.finish_time is not None
        assert job_log.start_time < job_log.finish_time

        session.commit()
