import pytest

from worker.main import init_log
from worker.main import init_app

# init_log()
application = init_app()


@pytest.fixture
def app():
    return application
