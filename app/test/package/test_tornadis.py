from tornado import gen
import tornadis
import pytest

REDIS_HOST='redis'
REDIS_PORT=6379

REDIS_TEST_CHANNEL='qconvert:test'


@pytest.fixture
def client():
    client = tornadis.Client(host=REDIS_HOST, port=REDIS_PORT, autoconnect=True)
    return client


@gen.coroutine
def redis_send_message(client):
    yield gen.sleep(0.5)

    yield client.call('PUBLISH', REDIS_TEST_CHANNEL, b'hello_world1')
    yield client.call('PUBLISH', REDIS_TEST_CHANNEL, b'hello_world2')
    yield client.call('PUBLISH', REDIS_TEST_CHANNEL, b'hello_world3')
    yield client.call('PUBLISH', REDIS_TEST_CHANNEL, b'hello_world4')


@pytest.mark.gen_test_current
def test_redis_ping(client):
    result = yield client.call('PING')

    assert result.decode('utf8') == 'PONG'


@pytest.mark.gen_test_current
def test_redis_pub_sub(client):
    sub = tornadis.PubSubClient(host=REDIS_HOST, port=REDIS_PORT,
                                autoconnect=True)
    # yield sub.connect()

    res = yield sub.pubsub_subscribe(REDIS_TEST_CHANNEL)
    assert res is True

    yield redis_send_message(client)

    # msg format
    # ['message', 'channel', 'message']
    # ex: [b'message', b'qconvert:test', b'hello_world1']
    msgs = yield [sub.pubsub_pop_message(),
                  sub.pubsub_pop_message(),
                  sub.pubsub_pop_message(),
                  sub.pubsub_pop_message()]

    real_msgs = [m[2].decode('utf8') for m in msgs]
    assert real_msgs == ['hello_world1', 'hello_world2',
                         'hello_world3', 'hello_world4']
