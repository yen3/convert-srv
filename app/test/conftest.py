from datetime import datetime

import pytest

from common.db.general import session_context
from common.db.worker import Worker
from common.db.job import Job, JobLog
from common.db.task import Task


def pytest_addoption(parser):
    parser.addoption('--slow', action='store_true', default=False,
                     help='Also run slow tests')
    parser.addoption('--integration', action='store_true', default=False,
                     help='Run integraion tests only')


def pytest_runtest_setup(item):
    """Skip tests if they are marked as slow and --slow is not given"""

    if item.get_marker('slow') and not item.config.getvalue('slow'):
        pytest.skip('slow test not reuqest')

    if item.get_marker('integration'):
        if not item.config.getvalue('integration'):
            pytest.skip('Only run in integration test')
    else:
        if item.config.getvalue('integration'):
            pytest.skip('Not run for integration test')


def reset_db_tables():
    with session_context() as session:
        session.query(Task).delete()
        session.query(Job).delete()
        session.query(JobLog).delete()
        session.query(Worker).delete()
        session.commit()

    from common.db.general import ENGINE
    with ENGINE.connect() as conn:
        conn.execute("SELECT setval('task_id_seq',1)")
        conn.execute("SELECT setval('job_id_seq',1)")
        conn.execute("SELECT setval('job_log_id_seq',1)")
        conn.execute("SELECT setval('worker_id_seq',1)")


def insert_test_worker_db_data():
    # task id is 2
    task_data = [
        {
            "name": "test_task_1",
            "work_dir": "/mnt/data/workspace/task_2",
            "task_type": "ffmpeg",
            "cmd": "ffmpeg",
            "cmd_options": "-c:v libx264 -c:a copy",
            "remove_temp": False,
            "split_unit": 10,
            "status": "creating",
        },
    ]
    # job_id is 2, 3, 4, 5, 6
    job_data = [
        # Normal Case
        {
            "task_id": 2,
            "input_path": "/mnt/data/workspace/task_2/input_01.txt",
            "output_path": "/mnt/data/workspace/task_2/output_01.txt",
            "cmd": "pwd",
            "cmd_options": "-L",
            "work_dir": "/mnt/data/workspace/task_2",
            "retry_limit": 2,
        },
        # Normal Case
        {
            "task_id": 2,
            "input_path": "/mnt/data/workspace/task_2/input_02.txt",
            "output_path": "/mnt/data/workspace/task_2/output_02.txt",
            "cmd": "echo",
            "cmd_options": "hello world job 2",
            "work_dir": "/mnt/data/workspace/task_2",
            "retry_limit": 1,
        },
        # Timeout Case
        {
            "task_id": 2,
            "input_path": "/mnt/data/workspace/task_2/input_02.txt",
            "output_path": "/mnt/data/workspace/task_2/output_02.txt",
            "cmd": "sleep",
            "cmd_options": "3",
            "timeout": "1",
            "work_dir": "/mnt/data/workspace/task_2",
            "retry_limit": 2,
        },
        # Fail Case
        {
            "task_id": 2,
            "input_path": "/mnt/data/workspace/task_2/input_02.txt",
            "output_path": "/mnt/data/workspace/task_2/output_02.txt",
            "cmd": "command_does_not_exist",
            "cmd_options": "in the environment",
            "timeout": "1",
            "work_dir": "/mnt/data/workspace/task_2",
            "retry_limit": 2,
        },
        # output path and cmd_options test case
        {
            "task_id": 2,
            "input_path": "/mnt/data/workspace/task_2/input_02.txt",
            "output_path": "/mnt/data/workspace/task_2/output_02_{{worker_id}}.txt",
            "cmd": "echo",
            "cmd_options": "output_02_{{worker_id}}.txt",
            "work_dir": "/mnt/data/workspace/task_2",
            "retry_limit": 1,
        },
    ]

    # job_log.id is 2
    job_logs_data = [
        {
            "job_id": 42,
            "retry_time": 0,
            "return_code": 0,
            "cmd": "echo",
            "cmd_options": "the answer",
            "stdout_log": "the answer",
            "stderr_log": "",
            "worker_id": 42,
            "error_msg": "",
            "is_timeout": False,
            "start_time": datetime.now(),
            "finish_time": datetime.now()
        }
    ]

    # workers.id is 2
    workers_data = [
        {
            "name": "test_ghost",
            "addr": "ghost.dev",
            "start_time": datetime.now(),
            "last_check_time": datetime.now(),
        }
    ]

    tasks = [Task(**t) for t in task_data]
    jobs = [Job(**j) for j in job_data]
    job_logs = [JobLog(**l) for l in job_logs_data]
    workers = [Worker(**w) for w in workers_data]

    with session_context() as session:
        session.add_all(tasks)
        session.add_all(jobs)
        session.add_all(job_logs)
        session.add_all(workers)
        session.commit()


def insert_test_server_db_data():
    # task ids are 2 and 3
    task_data = [
        {
            "name": "test_task_1",
            "input_path": "/mnt/data/trailer.mp4",
            "output_path": "/mnt/data/trailer_output.mp4",
            "work_dir": "/mnt/data/workspace/task_2",
            "task_type": "ffmpeg",
            "cmd": "ffmpeg",
            "cmd_options": "-c:v libx264 -c:a copy",
            "remove_temp": False,
            "split_unit": 10,
            "status": "creating",
        },
        {
            "name": "test_task_2",
            "input_path": "/mnt/data/trailer.mp4",
            "output_path": "/mnt/data/trailer_output.mp4",
            "work_dir": "/mnt/data/workspace/task_2",
            "task_type": "ffmpeg",
            "cmd": "ffmpeg",
            "cmd_options": "-c:v libx264 -c:a copy",
            "remove_temp": False,
            "status": "ready",
        },
    ]
    # jobs id are 2, 3, 4 and 5
    job_data = [
        # 2
        {
            "task_id": 42,
            "input_path": "/mnt/data/workspace/task_2/input_01.txt",
            "output_path": "/mnt/data/workspace/task_2/output_01.txt",
            "cmd": "pwd",
            "cmd_options": "-L",
            "work_dir": "/mnt/data/workspace/task_2",
            "retry_limit": 2,
        },
        # 3
        {
            "task_id": 3,
            "worker_id": 1,
            "input_path": "/mnt/data/workspace/task_2/task_2_input_0000000000.mp4",
            "output_path": "/mnt/data/workspace/task_2/task_2_0000000000_1.mp4",
            "cmd": "ffmpeg",
            "cmd_options": "-i /mnt/data/workspace/task_2/task_2_input_0000000000.mp4 -c:v libx264 -c:a copy /mnt/data/workspace/task_2/task_2_0000000000_1.mp4",
            "work_dir": "/mnt/data/workspace/task_2",
            "redirect_stdout_err": True,
            "status": "ready",
            "fail_reason": "done",
            "timeout": 0,
            "start_time": datetime.now(),
            "finish_time": datetime.now(),
            "retry_time": 1,
            "retry_limit": 1,
        },
        # 4
        {
            "task_id": 3,
            "worker_id": 2,
            "input_path": "/mnt/data/workspace/task_2/task_2_input_0000000001.mp4",
            "output_path": "/mnt/data/workspace/task_2/task_2_0000000001_2.mp4",
            "cmd": "ffmpeg",
            "cmd_options": "-i /mnt/data/workspace/task_2/task_2_input_0000000001.mp4 -c:v libx264 -c:a copy /mnt/data/workspace/task_2/task_2_0000000001_2.mp4",
            "work_dir": "/mnt/data/workspace/task_2",
            "redirect_stdout_err": True,
            "status": "ready",
            "fail_reason": "done",
            "timeout": 0,
            "start_time": datetime.now(),
            "finish_time": datetime.now(),
            "retry_time": 1,
            "retry_limit": 1,
        },
        # 5
        {
            "task_id": 3,
            "worker_id": 3,
            "input_path": "/mnt/data/workspace/task_2/task_2_input_0000000002.mp4",
            "output_path": "/mnt/data/workspace/task_2/task_2_0000000002_3.mp4",
            "cmd": "ffmpeg",
            "cmd_options": "-i /mnt/data/workspace/task_2/task_2_input_0000000002.mp4 -c:v libx264 -c:a copy /mnt/data/workspace/task_2/task_2_0000000002_3.mp4",
            "work_dir": "/mnt/data/workspace/task_2",
            "redirect_stdout_err": True,
            "status": "ready",
            "fail_reason": "done",
            "timeout": 0,
            "start_time": datetime.now(),
            "finish_time": datetime.now(),
            "retry_time": 1,
            "retry_limit": 1,
        },
    ]
    job_logs_data = [
        {
            "job_id": 42,
            "retry_time": 0,
            "return_code": 0,
            "cmd": "echo",
            "cmd_options": "the answer",
            "stdout_log": "the answer",
            "stderr_log": "",
            "worker_id": 42,
            "error_msg": "",
            "is_timeout": False,
            "start_time": datetime.now(),
            "finish_time": datetime.now()
        }
    ]
    workers_data = [
        {
            "name": "test_ghost",
            "addr": "ghost.dev",
            "start_time": datetime.now(),
            "last_check_time": datetime.now(),
        }
    ]


    tasks = [Task(**t) for t in task_data]
    jobs = [Job(**j) for j in job_data]
    job_logs = [JobLog(**l) for l in job_logs_data]
    workers = [Worker(**w) for w in workers_data]

    with session_context() as session:
        session.add_all(tasks)
        session.add_all(jobs)
        session.add_all(job_logs)
        session.add_all(workers)
        session.commit()


@pytest.yield_fixture
def db():
    reset_db_tables()

    yield

    reset_db_tables()


@pytest.yield_fixture
def db_worker(db):
    insert_test_worker_db_data()

    return db


@pytest.fixture
def db_server(db):
    insert_test_server_db_data()

    return db
