from datetime import datetime

from tornado import gen
from tornado.escape import json_decode
import pytest

from common.db.general import session_context
from common.db.job import Job, JobLog, JobStatus, get_job
from common.db.worker import Worker, WorkerStatus, get_all_worker_ids
from common.db.worker import get_worker

integration_test = pytest.mark.integration


@integration_test
def test_integration_get_worker_infomation():
    worker_ids = get_all_worker_ids()
    assert worker_ids == [1, 2]

    for worker_id in worker_ids:
        worker = get_worker(worker_id)

        assert worker.name is not None
        assert worker.addr is not None
        assert worker.start_time is not None

        if worker.last_check_time:
            assert worker.start_time < worker.last_check_time


@integration_test
@pytest.mark.gen_test
def test_integration_ping_worker(http_client):
    url = "http://{addr}:11000/api/v1/"
    worker_ids = get_all_worker_ids()

    for worker_id in worker_ids:
        worker = get_worker(worker_id)

        ping_url = url.format(addr=worker.addr)
        resp = yield http_client.fetch(ping_url)
        data = json_decode(resp.body.decode('utf8'))
        assert data == {"status": "success",
                        "data": "worker is running"}


@pytest.yield_fixture
def job_data():
    job_data = [
        # Normal Case
        {"task_id": 42,
         "input_path": "/mnt/data/workspace/task_2/input_01.txt",
         "output_path": "/mnt/data/workspace/task_2/output_01.txt",
         "cmd": "pwd",
         "cmd_options": "-L",
         "work_dir": "/mnt/data/workspace/task_2",
         "start_time": datetime.now(),
         "retry_limit": 2,
        },
        # Normal Case
        {"task_id": 42,
         "input_path": "/mnt/data/workspace/task_2/input_02.txt",
         "output_path": "/mnt/data/workspace/task_2/output_02.txt",
         "cmd": "echo",
         "cmd_options": "hello world job 2",
         "work_dir": "/mnt/data/workspace/task_2",
         "start_time": datetime.now(),
         "retry_limit": 1,
        },
    ]

    jobs = [Job(**j) for j in job_data]

    with session_context() as session:
        session.add_all(jobs)
        session.commit()

        job_ids = [j.id for j in jobs]

    yield (42, job_ids)

    with session_context() as session:
        session.query(Job).filter(Job.task_id == 42).delete()
        session.query(JobLog).filter(JobLog.task_id == 42).delete()
        session.commit()


@integration_test
@pytest.mark.gen_test
def test_integration_worker_run_job(job_data):
    task_id, job_ids = job_data

    yield gen.sleep(3)

    for job_id in job_ids:
        job = get_job(job_id)

        assert job.task_id == task_id
        assert job.status == JobStatus.done.value
        assert job.retry_time == 1
        assert job.start_time < job.finish_time


    with session_context() as session:
        query = session.query(JobLog).filter(JobLog.task_id == task_id)
        job_logs = query.order_by(JobLog.job_id).all()
        session.flush()

        for job_log in job_logs:
            session.expunge(job_log)

        session.commit()

    assert len(job_logs) == 2
    for job_log in job_logs:
        assert job_log.task_id == task_id
        assert job_log.job_id in job_ids

    job1 = get_job(job_ids[0])
    job_log1 = job_logs[0]
    assert job_log1.job_id == job1.id
    assert job_log1.input_path == job1.input_path
    assert job_log1.output_path == job1.output_path
    assert job_log1.stdout_log == job1.work_dir + "\n"
    assert job_log1.work_dir == job1.work_dir
    assert job_log1.stderr_log == ""
    assert job_log1.is_timeout is False
    assert job_log1.error_msg == ""
    assert job_log1.start_time < job_log1.finish_time

    job2 = get_job(job_ids[1])
    job_log2 = job_logs[1]
    assert job_log2.job_id == job2.id
    assert job_log2.input_path == job2.input_path
    assert job_log2.output_path == job2.output_path
    assert job_log2.stdout_log == "hello world job 2\n"
    assert job_log2.work_dir == job2.work_dir
    assert job_log2.stderr_log == ""
    assert job_log2.is_timeout is False
    assert job_log2.error_msg == ""
    assert job_log2.start_time < job_log2.finish_time

    yield gen.sleep(1)
