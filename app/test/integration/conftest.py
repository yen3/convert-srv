from datetime import datetime

import pytest
import tornado.web


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


# init_log()
application = tornado.web.Application([(r"/", MainHandler), ])


@pytest.fixture
def app():
    return application

