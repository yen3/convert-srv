from datetime import datetime
import os
import os.path

from tornado import gen
from tornado.httpclient import HTTPRequest
from tornado.escape import json_encode, json_decode
import pytest

from common.db.general import session_context
from common.db.task import Task, TaskStatus, get_task
from common.db.job import Job, JobLog, JobStatus, get_jobs_by_task_id


integration_test = pytest.mark.integration


def check_jobs(task_id):
    jobs = get_jobs_by_task_id(task_id)

    if any([job.status == JobStatus.fail.value for job in jobs]):
        return False

    valid_running_states = [JobStatus.ready.value, JobStatus.running.value]
    if any([job.status in valid_running_states for job in jobs]):
        return True

    if all([job.status == JobStatus.done.value for job in jobs]):
        return False


@integration_test
@pytest.mark.gen_test(timeout=300)
@pytest.mark.parametrize("path,expected_path",[
    ({'input_path': 'trailer_short.mp4',
      'output_path': 'trailer_short_output.mp4'},
     {'input_path': '/mnt/data/trailer_short.mp4',
      'output_path': '/mnt/data/trailer_short_output.mp4'}),
    ({'input_path': 'trailer_short.mp4'},
     {'input_path': '/mnt/data/trailer_short.mp4',
      'output_path': '/mnt/data/trailer_short-h264.mp4'}),
])
def test_integration_create_delete_task(http_client, path, expected_path):
    try:
        os.remove(expected_path['output_path'])
    except Exception:
        pass

    base_url = "http://localhost:11100/api/v1/"
    request_data = {"name": "trailer_convert",
                    "input_path": path['input_path'],
                    "cmd": "ffmpeg",
                    "cmd_options": "-c:v libx264 -c:a copy -y",
                    "split_unit": 10,
                    "remove_temp": False}
    if 'output_path' in path:
        request_data['output_path'] = path['output_path']

    url = os.path.join(base_url, "task")
    request = HTTPRequest(url=url, method="POST",
                          body=json_encode(request_data))
    response = yield http_client.fetch(request)
    data = json_decode(response.body.decode('utf8'))
    task_id = data['data']['id']
    assert task_id >= 0

    task = get_task(task_id)
    assert task.input_path == expected_path['input_path']
    assert task.output_path == expected_path['output_path']
    assert task.cmd == request_data['cmd']
    assert task.cmd_options == request_data['cmd_options']
    assert task.remove_temp == request_data['remove_temp']
    assert task.status in [TaskStatus.creating.value, TaskStatus.ready.value,
                           TaskStatus.running.value]
    assert task.create_time <= datetime.now()
    if task.start_time:
        assert task.create_time <= task.start_time

    # Wait for jobs are created.
    jobs = []
    while len(jobs) != 3:
        jobs = get_jobs_by_task_id(task_id)
        print("Wait for job creating ...")
        yield gen.sleep(1)

    for job in jobs:
        assert job.status in [JobStatus.ready.value, JobStatus.running.value]

    # Wait for jobs are done.
    elapsed_secs = 0
    while check_jobs(task_id) is True:
        print("Wait for job running {secs} ...".format(secs=elapsed_secs))
        task = get_task(task_id)
        assert task.status in [TaskStatus.running.value,
                               TaskStatus.ready.value]

        yield gen.sleep(5)
        elapsed_secs = elapsed_secs + 5

    # Check all jobs is done
    jobs = get_jobs_by_task_id(task_id)
    assert all([job.status == JobStatus.done.value for job in jobs])
    for job in jobs:
        assert job.start_time is not None
        assert job.finish_time is not None
        assert job.start_time < job.finish_time

    # Check all job logs
    job_logs = []
    with session_context() as session:
        for job in jobs:
            job_log = session.query(JobLog).filter(
                JobLog.job_id == job.id).one()
            session.flush()
            session.expunge(job_log)
            session.commit()
            job_logs.append(job_log)
    assert len(job_logs) == 3


    # Check task's status
    yield gen.sleep(5)
    task = get_task(task_id)
    assert task.status == TaskStatus.done.value
    assert task.remove_temp == request_data['remove_temp']

    # Check output file is exists
    assert os.path.isfile(expected_path['output_path'])

    # Check task's status through api
    response = yield http_client.fetch(
        os.path.join(base_url, "task", str(task_id)))

    data = json_decode(response.body.decode('utf8'))
    task_data = data['data']
    assert task_data['status'] == task.status
    assert task_data['input_path'] == task.input_path[len("/mnt/data/"):]
    assert task_data['output_path'] == task.output_path[len("/mnt/data/"):]
    assert task_data['remove_temp'] == task.remove_temp
    assert task_data['create_time'] != ""
    assert task_data['start_time'] != ""
    assert task_data['finish_time'] != ""

    # Compare time series
    timestamps = [datetime.strptime(task_data[key], "%Y-%m-%d %H:%M:%S")
                  for key in ['create_time', 'start_time', 'finish_time']]
    for prev_time, next_time in zip(timestamps, timestamps[1:]):
        assert prev_time <= next_time

    # Delete the task
    delete_url = os.path.join(base_url, "task", str(task_id))
    request = HTTPRequest(url=delete_url, method="DELETE")
    response = yield http_client.fetch(request)
    assert response.code == 200
    data = json_decode(response.body.decode('utf8'))
    assert data == {'status': 'success',
                    'data': {'delete_status': 'success',
                             'id': task_id}}

    # Check no record for the task
    print("Wait for the task are removed")
    yield gen.sleep(5)

    with session_context() as session:
        task = session.query(Task).filter(Task.id == task_id).first()
        jobs = session.query(Job).filter(Job.task_id == task_id).all()
        job_logs = session.query(JobLog).filter(
            JobLog.task_id == task_id).all()
        session.flush()
        session.commit()

        assert task == None
        assert jobs == []
        assert job_logs == []
