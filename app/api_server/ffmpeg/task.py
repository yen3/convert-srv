import os
import os.path
import shlex
import subprocess
import shutil

from common.db.general import session_context
from common.db.job import Job, JobStatus, FailStatus
from common.db.job import get_jobs_by_task_id
from common.db.task import Task, TaskStatus
from common.utils import file_suffix

MIN_SPLIT_UNIT = 10 # secs
SPLIT_UNIT = 300  # secs


def _get_suffix(path):
    suffix = file_suffix(path)
    return suffix if suffix else ".mp4"


def _calculate_split_unit(task_split_unit):
    if task_split_unit == 0:
        return SPLIT_UNIT

    return MIN_SPLIT_UNIT if task_split_unit < MIN_SPLIT_UNIT else \
            int(task_split_unit)


def _generate_split_cmd(input_path, output_format, split_unit):
    CMD_TEMPLATE = ('ffmpeg -i {input_path} '
                    '-map 0 -c:v copy -c:a copy -f segment '
                    '-segment_time {split_secs} -v error -y '
                    '"{output_format}"')

    return CMD_TEMPLATE.format(input_path=input_path,
                               split_secs=split_unit,
                               output_format=output_format)


def _generate_split_cmd_output_format(task_id, input_path, work_dir):
    suffix = _get_suffix(input_path)
    output_prefix = "task_{task_id}_input".format(task_id=task_id)
    output_format = os.path.join(work_dir, "{prefix}_%010d{suffix}".format(
        prefix=output_prefix, suffix=suffix))

    return output_prefix, output_format


def _split_ffmpeg_input_file(task):
    work_dir = task.work_dir

    # Get split unit
    split_unit = _calculate_split_unit(task.split_unit)

    # Generate split ffmpeg command
    output_prefix, output_format = _generate_split_cmd_output_format(
        task.id, task.input_path, work_dir)
    cmd = _generate_split_cmd(task.input_path, output_format, split_unit)

    # Run split ffmpeg command
    subprocess.check_call(shlex.split(cmd))

    # Collect split files
    files = [os.path.join(work_dir, fn) for fn in os.listdir(work_dir)
             if fn.startswith(output_prefix)]
    files.sort()

    return files


def _create_jobs_for_task(task, files):
    suffix = _get_suffix(task.output_path)
    output_path_template = os.path.join(
        task.work_dir, "task_%d_%010d_{{worker_id}}" + suffix)
    cmd_options_template = "-i %s " + task.cmd_options + " "
    cmd_options_template += output_path_template

    # Create job objects
    jobs = []
    for n, ifn in enumerate(files):
        cmd_options = cmd_options_template % (ifn, task.id, n)
        output_path = output_path_template % (task.id, n)

        job = Job()
        job.task_id = task.id
        job.input_path = ifn
        job.output_path = output_path
        job.work_dir = task.work_dir
        job.cmd = task.cmd
        job.cmd_options = cmd_options
        job.redirect_stdout_err = True
        job.timeout = task.timeout
        job.retry_limit = task.job_retry_limit

        jobs.append(job)

    # Add jobs to database
    with session_context() as session:
        session.add_all(jobs)
        session.commit()

    split_unit = _calculate_split_unit(task.split_unit)
    if split_unit != task.split_unit:
        with session_context() as session:
            task = session.query(Task).filter(Task.id == task.id).one()
            task.split_unit = split_unit
            session.commit()


def init_task(task):
    # 1. Create working folder
    work_dir = task.work_dir
    if not os.path.isdir(work_dir):
        os.makedirs(work_dir)

    # 2. Generate split files
    files = _split_ffmpeg_input_file(task)

    # 3. Create jobs for the task
    _create_jobs_for_task(task, files)

    return True


def merge_task(task):
    task_id = task.id

    # 1. Get all jobs of the task
    jobs = get_jobs_by_task_id(task_id)

    # 2. Create output file list into a file for ffmpeg
    files = [job.output_path for job in jobs]

    merge_fn = os.path.join(task.work_dir, 'file_list.txt')
    with open(merge_fn, 'w') as fin:
        for fn in files:
            fin.write("file '{fn}'\n".format(fn=fn))

    # 3. Run merge command
    cmd = ('ffmpeg -f concat -safe 0 '
           '-i {merge_input} -c copy -y '
           '{output_path}')
    cmd = cmd.format(merge_input=merge_fn, output_path=task.output_path)
    subprocess.check_call(shlex.split(cmd))

    # 4. Remove the working folder (if needed)
    if task.remove_temp and os.path.isdir(task.work_dir):
        shutil.rmtree(task.work_dir)

    return True
