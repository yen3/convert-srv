from enum import Enum
import itertools

from tornado import gen
from tornadis import TornadisException

from api_server.config import app_log
from api_server.config import REDIS_HOST, REDIS_PORT
from common.redis import RedisSubscriptor


class TaskEvent(Enum):
    create_task = 'create_task'
    remove_task = 'remove_task'
    merge_task = 'merge_task'
    running_task = 'running_task'
    fail_task = 'fail_task'


class EventHandleDaemon(object):
    """
    Handle event for redis subscriptor
    """
    def __init__(self, redis_channel, sleep_secs=2):
        self._sleep_secs = sleep_secs
        self._funcs = []

        self._redis_channel = redis_channel
        self._client = RedisSubscriptor(self._redis_channel,
                                        host=REDIS_HOST,
                                        port=REDIS_PORT,
                                        autoconnect=True)

    def register(self, fun):
        if fun not in self._funcs:
            self._funcs.append(fun)

    def _is_run(self, monitor_events, event_max):
        # Always run
        if event_max == 0:
            return True

        return monitor_events < event_max

    @gen.coroutine
    def run(self, event_max=0):
        monitor_events = 0

        yield self._client.connect()

        while self._is_run(monitor_events, event_max):
            app_log.debug("Wait for {channel}'s message".format(
                channel=self._redis_channel))
            msg = yield self._client.subscribe_json(
                sleep_secs=self._sleep_secs)

            # No new mssage
            if isinstance(msg, TornadisException):
                continue

            # Receive from redis
            handle_id = msg['message']['id']
            request = msg['message']

            # Dispatch events
            for fun in self._funcs:
                yield fun(handle_id, request)

            monitor_events = monitor_events + 1


def get_daemon_functions():
    from .job import get_daemon_functions as get_j
    from .task import get_daemon_functions as get_t
    from .worker import get_daemon_functions as get_w

    funs = itertools.chain(*[get_j(), get_t(), get_w()])
    return funs
