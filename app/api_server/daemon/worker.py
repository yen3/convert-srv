from datetime import datetime, timedelta
import os.path

from tornado import gen
from tornado.gen import TimeoutError
from tornado.escape import json_decode
from tornado.httputil import url_concat
from tornado.httpclient import AsyncHTTPClient, HTTPRequest

from common.redis import RedisClient
from common.db.general import session_context
from common.db.worker import Worker, WorkerStatus
from common.db.worker import get_worker, get_all_workers, get_all_worker_ids
from api_server.config import app_log
from api_server.config import REDIS_DATA_PREFIX, REDIS_HOST, REDIS_PORT


CHECK_SECONDS = 20                    # secs
CHECK_PERIOD = 10                     # secs
COLLECT_WORKER_STATUS_SLEEP_SECS = 3  # secs

REDIS_DATA_WORKER_PREIFX = "%s:worker" % (REDIS_DATA_PREFIX)


def _get_ping_url(addr, port=None):
    if port is None:
        port = 11000

    url = 'http://{addr}:{port}/api/v1/'.format(addr=addr, port=port)
    return url


@gen.coroutine
def ping_worker(addr):
    try:
        url = _get_ping_url(addr)
        app_log.debug(url)

        http_client = AsyncHTTPClient()
        request = HTTPRequest(url, connect_timeout=10, request_timeout=10)
        response = yield http_client.fetch(request)

        raw_data = json_decode(response.body.decode('utf8'))
        data = raw_data['data']

        return 'running' in data

    except TimeoutError as err:
        app_log.warning(err)
        return False

    except Exception as err:
        app_log.error(err)
        return False

    finally:
        http_client.close()


@gen.coroutine
def check_worker_alive(worker_id):
    # get work data
    worker = get_worker(worker_id)

    # Check current worker status
    valid_worker_status = [WorkerStatus.ready.value,
                           WorkerStatus.running.value]
    if worker.status not in valid_worker_status:
        return

    # if the check time is not expired, continue
    expired_time = worker.last_check_time + timedelta(seconds=CHECK_SECONDS)
    if expired_time > datetime.now():
        return

    ping_status = yield ping_worker(worker.addr)
    if ping_status:
        # update last check time
        with session_context() as session:
            worker = session.query(Worker).filter(Worker.id == worker_id).one()
            worker.last_check_time = datetime.now()
            session.commit()

    else:
        # change the worker status to fail
        with session_context() as session:
            query = session.query(Worker).filter(Worker.id == worker_id)
            query = query.with_for_update()
            worker = query.one()
            worker.status = WorkerStatus.fail.value
            worker.last_check_time = datetime.now()

            session.commit()

        app_log.info("Check worker id: {worker_id} fail".format(
            worker_id=worker_id))

@gen.coroutine
def work_check_alive_daemon_internal(sleep_secs):
    worker_ids = get_all_worker_ids()
    app_log.debug("Watch current workers: " + str(worker_ids))

    for worker_id in worker_ids:
        yield check_worker_alive(worker_id)

    app_log.debug("Watch done, enter to sleep")
    yield gen.sleep(sleep_secs)


@gen.coroutine
def work_check_alive_daemon():
    while True:
        yield work_check_alive_daemon_internal(CHECK_PERIOD)


def retrive_unique_worker_info():
    raw_workers = get_all_workers()
    workers = dict()

    for worker in raw_workers:
        if worker.name in workers:
            app_log.warning(
                "duplicate worker name: {name}, addr: {addr}".format(
                name=worker.name, addr=worker.addr))

        info = {
            'id': worker.id,
            'name': worker.name,
            'addr': worker.addr,
            'start_time': worker.start_time.strftime("%Y-%m-%d %H:%M:%S"),
            'last_check_time': worker.last_check_time.strftime("%Y-%m-%d %H:%M:%S"),
            'status': worker.status
        }

        workers[worker.name] = info

    return workers


@gen.coroutine
def save_worker_info(workers):
    redis = RedisClient(host=REDIS_HOST, port=REDIS_PORT)
    yield redis.connect()

    # Remove previous worker information
    keys = yield redis.keys("%s:*" % REDIS_DATA_WORKER_PREIFX)
    if keys:
        new_keys = ["%s:%s" % (REDIS_DATA_WORKER_PREIFX, k)
                    for k in workers.keys()]
        del_keys = [k for k in keys if k not in new_keys]
        yield redis.delete(*del_keys)

    # Save the current worker information
    for name, info in workers.items():
        yield redis.hmset("%s:%s" % (REDIS_DATA_WORKER_PREIFX, name), info)

    yield redis.disconnect()


@gen.coroutine
def collect_worker_status():
    workers = retrive_unique_worker_info()

    yield save_worker_info(workers)


@gen.coroutine
def collect_worker_status_daemon():
    while True:
        yield collect_worker_status()
        yield gen.sleep(COLLECT_WORKER_STATUS_SLEEP_SECS)


def get_daemon_functions():
    return [work_check_alive_daemon, collect_worker_status_daemon]
