from datetime import datetime, timedelta

from sqlalchemy.sql.expression import and_
from tornado import gen


from common.db.general import session_context
from common.db.task import Task, TaskStatus, get_all_task_ids, get_task_status
from common.db.job import Job, JobStatus, FailStatus, get_jobs_by_task_id
from api_server.config import app_log
from api_server.config import REDIS_JOB_CHANNEL
from .general import EventHandleDaemon, TaskEvent


# There are two events in server
# 1. Create/delete task from http request
# 2. The data in database is changed and the monitors detect.
#
# In condition 2, it's hard to send event only once through redis channel.
# The other possible way is to rewrite the mointor event with an observer.


job_daemon = None
job_handler = None


@gen.coroutine
def detect_task_running(task_id, jobs):
    if get_task_status(task_id) != TaskStatus.ready.value:
        return None

    if any([job.status == JobStatus.running.value for job in jobs]):
        return {"id": task_id, "event": TaskEvent.running_task.value}

    return None


@gen.coroutine
def detect_task_done(task_id, jobs):
    if get_task_status(task_id) in [TaskStatus.done.value,
                                    TaskStatus.fail.value]:
        return None

    # Sent a event to merge the task
    if all([job.status == JobStatus.done.value for job in jobs]):
        return {"id": task_id, "event": TaskEvent.merge_task.value}

    return None


@gen.coroutine
def detect_task_fail(task_id, jobs):
    if get_task_status(task_id) == TaskStatus.fail.value:
        return None

    if any([job.status == JobStatus.fail.value for job in jobs]):
        return {"id": task_id, "event": TaskEvent.fail_task.value}

    return None


def _fail_job(job_id, check_status):
    with session_context() as session:
        query = session.query(Job)
        # Make sure the job is still running
        query = query.filter(and_(Job.id == job_id,
                                  Job.status == check_status))
        # Avoid race condition with worker's update
        query = query.with_for_update()
        job = query.first()

        # If the job's status is still running, change to fail/fail_retry
        if job:
            next_status = JobStatus.fail_retry.value \
                if job.retry_time < job.retry_limit else \
                JobStatus.fail.value
            job.status = next_status
            job.fail_reason = FailStatus.timeout.value
            app_log.debug("Set job (id: {job_id}) to fail status.".format(
                job_id=job.id))

        session.commit()


@gen.coroutine
def detect_task_timeout_without_worker_stop(task_id, jobs):
    # The purpose of the function is just to detect timeout job and change the
    # jobs status
    DETECT_TIMEOUT_ERROR = 60            # secs

    if get_task_status(task_id) != TaskStatus.running.value:
        return None

    for job in jobs:
        # No timeout or Not running
        if job.timeout == 0 or job.status != JobStatus.running.value:
            continue

        detect_timeout = job.timeout + DETECT_TIMEOUT_ERROR
        dead_time = job.start_time + timedelta(seconds=detect_timeout)

        # Still alive, wating ...
        if dead_time > datetime.now():
            continue

        # Change the job's status carefully
        job_id = job.id
        _fail_job(job.id, JobStatus.running.value)

        # TODO: if the job is timeout in the worker. There are several
        # possible reasons
        #
        # 1. The job is still running but unfinished.
        # 2. The worker is dead.
        # 3. The worker try to write the database entry but failed.
        # ... etc
        #
        # It's very hard to know what the real problem is, does the
        # function have any need to kill worker or restart worker ?
        # The function would not implement in the beta. If the site would
        # be a real product, we have to consider the problem seriously.

    # No event to send, if there is any job whose status is changed to fail,
    # `detect_task_fail` function will send the event
    return None


class JobDaemon(object):
    def __init__(self, notify_handler, sleep_secs=3):
        self._funcs = []
        self._handler = notify_handler
        self._sleep_secs = sleep_secs

    def register(self, fun):
        if fun not in self._funcs:
            self._funcs.append(fun)

    @gen.coroutine
    def _notify(self, request):
        yield self._handler.notify(request)

    def _is_run(self, deal_events, event_max):
        if event_max == 0:
            return True

        return deal_events < event_max

    @gen.coroutine
    def _monitor(self, task_id, jobs):
        has_event = False
        for func in self._funcs:
            request = yield func(task_id, jobs)
            if request:
                has_event = True
                yield self._notify(request)

        return has_event

    @gen.coroutine
    def run(self, event_max=0):
        deal_events = 0

        while self._is_run(deal_events, event_max):
            task_ids = get_all_task_ids()
            # app_log.debug("Monitor all tasks: " + str(task_ids))

            has_new_event = False

            for task_id in task_ids:
                jobs = get_jobs_by_task_id(task_id)
                new_event = yield self._monitor(task_id, jobs)

                has_new_event = True if new_event else has_new_event

            if has_new_event:
                deal_events = deal_events + 1

            yield gen.sleep(self._sleep_secs)


class JobEventHandler(object):
    def __init__(self):
        self._funcs = []

    def register(self, func):
        if func not in self._funcs:
            self._funcs.append(func)

    @gen.coroutine
    def notify(self, request):
        app_log.debug("JobDaemon new event: " + str(request))
        for func in self._funcs:
            yield func(request['id'], request)


def get_daemon_functions():
    from api_server.daemon.task import merge_task_output
    from api_server.daemon.task import set_task_running
    from api_server.daemon.task import fail_task

    global job_daemon
    global job_handler

    if job_handler is None:
        job_handler = JobEventHandler()
        job_handler.register(merge_task_output)
        job_handler.register(fail_task)
        job_handler.register(set_task_running)

    if job_daemon is None:
        job_daemon = JobDaemon(job_handler, sleep_secs=2)

        # The order of functions has meaning, don't change the order of these
        # functions in free unless you know what you do

        job_daemon.register(detect_task_running)
        job_daemon.register(detect_task_done)
        # In the function, it's possible to change some job's status to fail
        # if happens, the `detect_task_fail` will send events
        job_daemon.register(detect_task_timeout_without_worker_stop)
        job_daemon.register(detect_task_fail)

    return [job_daemon.run]
