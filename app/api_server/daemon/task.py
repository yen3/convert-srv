from tornado import gen
from sqlalchemy.sql.expression import and_

from common.db.general import session_context
from common.db.task import TaskStatus
from common.db.task import get_task, get_task_status, change_task_status
from common.db.task import delete_task
from common.db.job import Job, JobLog, JobStatus, FailStatus
from api_server.config import app_log
from api_server.config import REDIS_TASK_CHANNEL, REDIS_JOB_CHANNEL
from api_server.ffmpeg.task import init_task as init_ffmpeg_task
from api_server.ffmpeg.task import merge_task as merge_ffmpeg_task
from .general import TaskEvent, EventHandleDaemon


task_event_handle_daemon = None

task_init_func = {"ffmpeg": init_ffmpeg_task}
task_merge_func = {"ffmpeg": merge_ffmpeg_task}


@gen.coroutine
def init_task(task_id, request):
    global task_init_func

    if request['event'] != TaskEvent.create_task.value:
        return

    task = get_task(task_id)
    task_type = task.task_type

    if task_type not in task_init_func:
        raise NotImplementedError("Not support such task type to init")

    try:
        init_status = False
        init_status = task_init_func[task_type](task)
    except Exception as err:
        app_log.error(err)

    # Change the task's status
    next_status = TaskStatus.ready.value if init_status else \
        TaskStatus.fail.value
    change_task_status(task_id, next_status)

    app_log.info("Init task id: {task_id} with status: {status}".format(
        task_id=task_id, status=next_status))


@gen.coroutine
def clean_finished_task(task_id, request):
    # If the requst is not for the event, ignore the request
    if request['event'] != TaskEvent.remove_task.value:
        return

    # If the task is not done or fail, ignore the request
    valid_remove_status = [TaskStatus.done.value,
                           TaskStatus.fail.value]
    if get_task_status(task_id) not in valid_remove_status:
        return

    # Delete all records
    delete_task(task_id)

    app_log.info("Clean task id: {task_id}".format(task_id=task_id))


@gen.coroutine
def merge_task_output(task_id, request):
    global task_merge_func

    if request['event'] != TaskEvent.merge_task.value:
        return

    task = get_task(task_id)
    task_type = task.task_type

    if task_type not in task_merge_func:
        raise NotImplementedError("Not support such task type to merge")

    try:
        merge_status = False
        merge_status = task_merge_func[task_type](task)
    except Exception as err:
        app_log.error(err)
        merge_status = False

    next_status = TaskStatus.done.value if merge_status else \
        TaskStatus.fail.value
    change_task_status(task_id, next_status)

    app_log.info("Merge task id: {task_id} with status: {status}".format(
        task_id=task_id, status=next_status))


@gen.coroutine
def set_task_running(task_id, request):
    if request['event'] != TaskEvent.running_task.value:
        return

    change_task_status(task_id, TaskStatus.running.value)

    app_log.info("Task id:{task_id} is running".format(task_id=task_id))


@gen.coroutine
def fail_task(task_id, request):
    if request['event'] != TaskEvent.fail_task.value:
        return

    # Set jobs of the task fail
    with session_context() as session:
        query = session.query(Job)
        query = query.filter(and_(Job.task_id == task_id,
                                  Job.status == JobStatus.ready.value))
        query = query.with_for_update()
        jobs = query.all()

        for job in jobs:
            job.status = JobStatus.fail.value
            job.fail_reason = FailStatus.cancel.value

        session.commit()

    # Set task fail
    change_task_status(task_id, TaskStatus.fail.value)

    app_log.info("Fail task id: {task_id} with status: {status}".format(
        task_id=task_id, status=TaskStatus.fail.value))


def get_daemon_functions():
    global task_event_handle_daemon

    if task_event_handle_daemon is None:
        task_event_handle_daemon = EventHandleDaemon(
            redis_channel=REDIS_TASK_CHANNEL)
        task_event_handle_daemon.register(init_task)
        task_event_handle_daemon.register(clean_finished_task)

    return [task_event_handle_daemon.run]
