import logging
import signal

from tornado.ioloop import IOLoop
from tornado_json.application import Application
import tornado.ioloop
import tornado.httpserver

from common.logging import init_log
from api_server.api.general import get_routes
from api_server.daemon.general import get_daemon_functions
from api_server.config import SERVER_PORT


def init_app():
    routes = get_routes()
    return Application(routes=routes, settings={'debug': True})


def sig_handler(sig, frame):
    logging.info('Server shutdown ...')
    IOLoop.current().stop()


def init_sig_handler():
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)


def init_daemons():
    daemon_funs = list(get_daemon_functions())

    io_loop = IOLoop.instance()
    for fun in daemon_funs:
        io_loop.spawn_callback(fun)


def main():
    logging.info('Start the api server')

    init_log()
    init_sig_handler()
    init_daemons()

    app = init_app()

    # Start the server
    logging.info('Server started on port %s ...' % (SERVER_PORT,))

    server = tornado.httpserver.HTTPServer(app)
    server.listen(SERVER_PORT)
    try:
        IOLoop.instance().start()
    finally:
        pass
