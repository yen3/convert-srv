import logging
import os.path

SERVER_PORT = 11100

DATA_FOLDER_ROOT = "/mnt/data"
TASK_WORK_DIR = os.path.join(DATA_FOLDER_ROOT, "workspace")

REDIS_HOST = 'redis'
REDIS_PORT = 6379

REDIS_TASK_CHANNEL = "qconvert:task"
REDIS_JOB_CHANNEL = "qconvert:job"
REDIS_WORKER_CHANNEL = "qconvert:worker"

REDIS_DATA_PREFIX = "qconvert:data"

app_log = logging.getLogger("tornado.application")

