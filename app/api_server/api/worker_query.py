from tornado_json.requesthandlers import APIHandler
from tornado_json import schema
from tornado import gen

from common.db.worker import Worker
from common.redis import RedisClient
from api_server.config import REDIS_HOST, REDIS_PORT
from api_server.daemon.worker import REDIS_DATA_WORKER_PREIFX
from api_server.api.general import fetch_db_object_dict
from api_server.api.general import fetch_db_objects_dict_list


@gen.coroutine
def fetch_redis_worker_info():
    client = RedisClient(host=REDIS_HOST, port=REDIS_PORT)
    yield client.connect()

    keys = yield client.keys("%s:*" % REDIS_DATA_WORKER_PREIFX)
    workers = yield [client.hgetall(k) for k in keys]

    yield client.disconnect()

    return workers


class WorkerListQueryHandler(APIHandler):
    @schema.validate(output_schema={
        "type": "array",
        "items": {
            "type": "object",
        }
    })
    @gen.coroutine
    def get(self):
        """
        Get all worker informations
        """
        workers = yield fetch_redis_worker_info()
        return workers


class WorkerQueryHandler(APIHandler):
    @schema.validate(output_schema={"type": "object", })
    def get(self, worker_id):
        """
        Get worker information
        """
        return fetch_db_object_dict(worker_id, Worker)
