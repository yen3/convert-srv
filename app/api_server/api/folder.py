import os
import os.path

from tornado_json.requesthandlers import APIHandler
from tornado_json import schema
from tornado import gen
from tornado.escape import url_unescape

from api_server.config import app_log, DATA_FOLDER_ROOT


class ListFolderFilesHandler(APIHandler):
    @schema.validate(output_schema={
        "type": "array",
        "items": {
            "type": "object",
            "required": ["path", "is_dir", "name"],
            "properties": {
                "path": {"type": "string"},
                "name": {"type": "string"},
                "is_dir": {"type": "boolean"},
            },
        }
    })
    def get(self, path):
        if path is None:
            abspath = DATA_FOLDER_ROOT
        else:
            abspath = os.path.join(DATA_FOLDER_ROOT, url_unescape(path))

        try:
            files = os.listdir(abspath)

        except Exception as err:
            app_log.error(err)
            files = []

        try:
            return [{"path": os.path.join('/', path, fn),
                     "name": fn,
                     "is_dir": os.path.isdir(os.path.join(abspath, fn))}
                    for fn in files]
        except Exception as err:
            app_log.error(err)
            return []

