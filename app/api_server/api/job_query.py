from tornado_json.requesthandlers import APIHandler
from tornado_json import schema
from tornado import gen

from common.db.general import session_context
from common.db.general import object_to_dict
from common.db.job import Job, JobLog
from common.db.job import get_jobs_by_task_id
from api_server.api.general import fetch_db_object_dict


class JobListQueryHandler(APIHandler):
    @schema.validate(output_schema={
        "type": "array",
        "items": {
            "type": "object",
        }
    })
    def get(self, task_id):
        """
        Get all job ids of a task, if the details is specified. return all job
        informations
        """
        jobs = get_jobs_by_task_id(task_id)

        return [object_to_dict(job, Job) for job in jobs]


class JobQueryHandler(APIHandler):
    @schema.validate(output_schema={"type": "object", })
    def get(self, job_id):
        """
        Get job information
        """
        return fetch_db_object_dict(job_id, Job)


class JobLogListQueryHandler(APIHandler):
    @schema.validate(output_schema={
        "type": "array",
        "items": {
            "type": "object",
        }
    })
    def get(self, job_id):
        """
        Get all job_log ids of a job, if the details is specified. return all
        job_log informations
        """
        with session_context() as session:
            query = session.query(JobLog).filter(JobLog.job_id == job_id)
            query = query.order_by(JobLog.id)
            logs = query.all()
            session.flush()
            print(logs)

            for log in logs:
                session.expunge(log)

            session.commit()

        return [object_to_dict(log, JobLog) for log in logs]


class JobLogQueryHandler(APIHandler):
    @schema.validate(output_schema={"type": "object", })
    def get(self, job_log_id):
        """
        Get job_log information
        """
        return fetch_db_object_dict(job_log_id, JobLog)
