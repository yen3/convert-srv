import os
import os.path

from tornado_json.requesthandlers import APIHandler
from tornado_json.exceptions import APIError
from tornado_json import schema
from tornado import gen

from common.db.general import session_context
from common.db.task import Task, TaskStatus
from common.db.task import create_task, task_dict, get_task, get_task_status
from common.redis import RedisPublisher
from common.utils import path_without_suffix, file_suffix
from api_server.config import DATA_FOLDER_ROOT, TASK_WORK_DIR
from api_server.config import REDIS_HOST, REDIS_PORT, REDIS_TASK_CHANNEL
from api_server.api.general import fetch_db_object_dict
from api_server.api.general import fetch_db_objects_dict_list
from api_server.daemon.general import TaskEvent


def _determine_task_type(request):
    task_type = 'ffmpeg' if 'ffmpeg' in request['cmd'] else None

    if task_type:
        request['task_type'] = task_type


@gen.coroutine
def _send_task_channel_event(request):
    client = RedisPublisher(REDIS_TASK_CHANNEL, host=REDIS_HOST,
                            port=REDIS_PORT, autoconnect=True)
    client.publish_json(request)
    yield client.disconnect()


@gen.coroutine
def _send_create_jobs(task_id):
    request = {'id': task_id, 'event': TaskEvent.create_task.value}
    yield _send_task_channel_event(request)


@gen.coroutine
def _send_delete_task(task_id):
    request = {'id': task_id, 'event': TaskEvent.remove_task.value}
    yield _send_task_channel_event(request)


def _generate_input_output_path(request):
    # If the output_path is not defined, give a default name
    if 'output_path' not in request:
        input_path = request['input_path']
        path = path_without_suffix(input_path)
        suffix = file_suffix(input_path)

        request['output_path'] = "{path}-h264{suffix}".format(
            path=path, suffix=suffix)

    # Join root folder path to input/output path
    for key in ['input_path', 'output_path']:
        request[key] = os.path.join(DATA_FOLDER_ROOT, request[key])


def _remove_input_output_folder_prefix(task):
    prefix = DATA_FOLDER_ROOT + "/"
    for key in ['input_path', 'output_path', 'work_dir']:
        if task[key].startswith(prefix):
            task[key] = task[key][len(prefix):]


class TaskHandler(APIHandler):
    @schema.validate(
        input_schema={
            "type": "object",
            "required": ["input_path", "cmd", "cmd_options"],
            "properties": {
                "name": {"type": "string"},
                "input_path": {"type": "string"},
                "output_path": {"type": "string"},
                "cmd": {"type": "string"},
                "cmd_options": {"type": "string"},
                "timeout": {"type": "number"},
                "split_unit": {"type": "number"},
                "remove_temp": {"type": "boolean"},
            },
        },
        output_schema={
            "type": "object",
            "required": ["id", "create_status"],
            "properties": {
                "id": {"type": "number"},                           # task id
                "create_status": {"type": "string"},
                "msg": {"type": "string"},
            },
        }
    )
    @gen.coroutine
    def post(self):
        request = self.body

        # Only support ffmpeg currently
        _determine_task_type(request)
        if request['task_type'] is None:
            return {"create_status": "fail",
                    "id": -1,
                    "msg": "does not support the command"}

        # Generate the absolute path for the request
        _generate_input_output_path(request)
        if not os.path.isfile(request['input_path']):
            return {"create_status": "fail",
                    "id": -1,
                    "msg": "No such input file"}

        # Create task in database
        request['status'] = TaskStatus.creating.value
        task_id = create_task(TASK_WORK_DIR, **request)
        if task_id is None:
            return {"create_status": "fail",
                    "id": -1,
                    "msg": "create task fail"}

        # Send the task number and creating request to daemon
        #   a. Create working folder
        #   b. Split the input files in working folder
        #   c. Create numbers of jobs to run
        yield _send_create_jobs(task_id)

        # Return ok and task id to the user
        return {"create_status": "success",
                "id": task_id,
                "msg": "create task success"}

    @schema.validate(output_schema={
        "type": "array",
        "items": {
            "type": "object",
        }
    })
    def get(self):
        """
        List all task ids, if the details is specified. return all task
        informations
        """
        tasks = fetch_db_objects_dict_list(Task)

        for task in tasks:
            _remove_input_output_folder_prefix(task)

        return tasks


class SingleTaskHandler(APIHandler):
    @schema.validate(output_schema={"type": "object", })
    def get(self, task_id):
        """
        Get task information
        """
        task = fetch_db_object_dict(task_id, Task)
        _remove_input_output_folder_prefix(task)

        return task

    @schema.validate(output_schema={
        "type": "object",
        "required": ["id", "delete_status"],
        "properties": {
            "delete_status": {"type": "string"},
            "id": {"type": "number"},                           # task id
            "msg": {"type": "string"},
        },
    })
    @gen.coroutine
    def delete(self, task_id):
        """
        Delete a task
        """
        task_id = int(task_id)

        valid_status = [TaskStatus.done.value, TaskStatus.fail.value]
        if get_task_status(task_id) not in valid_status:
            return {
                "delete_status": "fail",
                "id": task_id,
                "msg": "can not delete the task, the task is not done or fail",
            }


        yield _send_delete_task(task_id)

        return {
            "delete_status": "success",
            "id": task_id,
        }
