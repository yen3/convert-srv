from common.db.general import session_context
from common.db.general import object_to_dict


def get_routes():
    from .ping import PingHandler
    from .task import TaskHandler
    from .task import SingleTaskHandler
    from .job_query import JobQueryHandler
    from .job_query import JobLogQueryHandler
    from .job_query import JobListQueryHandler
    from .job_query import JobLogListQueryHandler
    from .worker_query import WorkerQueryHandler
    from .worker_query import WorkerListQueryHandler
    from .folder import ListFolderFilesHandler

    # Ref: Regex valid online - https://goo.gl/juotNz
    routes = [(r"/api/v1/", PingHandler),
              (r"/api/v1/task", TaskHandler),
              (r"/api/v1/task/(?P<task_id>\d+)", SingleTaskHandler),
              (r"/api/v1/joblog/(?P<job_log_id>\d+)", JobLogQueryHandler),
              (r"/api/v1/job/(?P<job_id>\d+)", JobQueryHandler),
              (r"/api/v1/task/(?P<task_id>\d+)/job", JobListQueryHandler),
              (r"/api/v1/job/(?P<job_id>\d+)/log", JobLogListQueryHandler),
              (r"/api/v1/worker", WorkerListQueryHandler),
              (r"/api/v1/worker/(?P<worker_id>\d+)", WorkerQueryHandler),
              (r"/api/v1/folder/(?P<path>.*)", ListFolderFilesHandler),
    ]

    return routes


def fetch_db_object_dict(obj_id, ObjType):
    if obj_id is None:
        return None

    with session_context() as session:
        obj = session.query(ObjType).filter(ObjType.id == obj_id).first()
        session.flush()

        if obj:
            session.expunge(obj)

        session.commit()

    if obj:
        return object_to_dict(obj, ObjType)
    else:
        return {}


def fetch_db_objects_dict_list(ObjType):
    with session_context() as session:
        objs = session.query(ObjType).order_by(ObjType.id).all()
        session.flush()

        for obj in objs:
            session.expunge(obj)

        session.commit()

    return [object_to_dict(obj, ObjType) for obj in objs]
