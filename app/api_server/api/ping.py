from tornado_json.requesthandlers import APIHandler
from tornado_json import schema
from tornado.web import MissingArgumentError


class PingHandler(APIHandler):
    @schema.validate(output_schema={"type": "string"})
    def get(self):
        return "video-convert is running"
