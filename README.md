# Transcoding service

* internal name: convert-srv

## Run in local

* Use docker-compose with `docker-compose-local.yml` to construct the service
* Command memo

    ```bash
    # Pull image first
    make -f Makefile.local pull-image

    # Shutdown the service
    make -f Makefile.local down

    # Start the service
    make -f Makefile.local up
    ```

## Test

* Unit test (120 tests)

    ```bash
    make -f Makefile.test down
    make -f Makefile.test up
    make -f Makefile.test test       # Skip some slow test cases
    make -f Makefile.test test-all   # Run longer than `test`
    make -f Makefile.test down
    ```

* Integration test (5 tests)

    ```bash
    make -f Makefile.local down
    make -f Makefile.local up
    make -f Makefile.local integration-test  # Need several minutes
    make -f Makefile.local down
    ```

## Run in Kubernetes

* Deploy

    ```sh
    kubectl apply -f ./k8s/0-namespace.yml
    kubectl apply -f ./k8s/storage.yml
    kubectl apply -f ./k8s/deployment.yml
    ```

* Remove 

    ```sh
    kubectl delete -f ./k8s/deployment.yml
    kubectl delete -f ./k8s/storage.yml
    kubectl delete -f ./k8s/0-namespace.yml
    ```

## Run in docker swarm

* TBD

