#!/bin/bash

docker-machine create \
  --driver generic \
  --generic-ip-address=192.168.180.176 \
  --generic-ssh-key ~/.ssh/id_rsa --generic-ssh-user=vagrant \
  leader

docker-machine create \
  --driver generic \
  --generic-ip-address=192.168.180.177 \
  --generic-ssh-key ~/.ssh/id_rsa \
  --generic-ssh-user=vagrant \
  node1

docker-machine create \
  --driver generic \
  --generic-ip-address=192.168.180.180 \
  --generic-ssh-key ~/.ssh/id_rsa \
  --generic-ssh-user=vagrant \
  node2
