# Build a test docker swarm vms

The default docker machine can not mount public IP's NFS so the manual notes
how to use vagrant vm as docker machine.

0. Install vagrant and virtualbox
1. Build vms `leader`, `node1` and `node2` through vagrant and virtualbox (You
   maybe to set the bridge interface in `Vargrantfile`.
   The default setting is `enp2s0`.)

    ```bash
    vagrant up
    ```

2. Add your account's public key to `leader`, `node1` and `node2`
3. Get the public IP from `leader`, `node1` and `node2`

    ```bash
    vagrant ssh leader "ip a"
    vagrant ssh node1 "ip a"
    vagrant ssh node2 "ip a"
    ```

4. Add `leader`, `node1` and `node2` to your docker-machine

    ```bash
    docker-machine create \
      --driver generic \
      --generic-ip-address=192.168.180.176 \
      --generic-ssh-key ~/.ssh/id_rsa \
      --generic-ssh-user=vagrant \
      leader

    docker-machine create \
      --driver generic \
      --generic-ip-address=192.168.180.177 \
      --generic-ssh-key ~/.ssh/id_rsa \
      --generic-ssh-user=vagrant \
      node1

    docker-machine create \
      --driver generic \
      --generic-ip-address=192.168.180.180 \
      --generic-ssh-key ~/.ssh/id_rsa \
      --generic-ssh-user=vagrant \
      node2
      ```

5. Now you have three docker-machines. Just follow
  [Offical docs](https://docs.docker.com/get-started/part4/) to set your
  swarm environment.
