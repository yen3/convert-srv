-- create types
-- https://stackoverflow.com/questions/7624919/check-if-a-user-defined-type-already-exists-in-postgresql
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'task_status') THEN
        CREATE TYPE task_status AS ENUM('ready', 'running', 'finished', 'timeout', 'fail');
    END IF;
END$$;


CREATE TABLE IF NOT EXISTS todo_tasks (
    id             SERIAL PRIMARY KEY,
    task_uuid      varchar(48),
    input_path     text,
    output_path    text,
    cmd            text,
    options        text            DEFAULT '',
    status         task_status     DEFAULT 'ready',
    timeout        integer         DEFAULT 0,
    machine        varchar(128),
    start_time     timestamp,
    finish_time    timestamp,
    retry_time     integer         DEFAULT 0,
    retry_limit    integer         DEFAULT 1,
    removed        boolean         DEFAULT false
);


CREATE TABLE IF NOT EXISTS task_logs (
    id            SERIAL PRIMARY KEY,
    task_id       integer references todo_tasks(id),
    retry_time    integer,
    return_code   integer,
    cmd           text,
    log_data      text,
    start_time    timestamp,
    finish_time   timestamp
);
