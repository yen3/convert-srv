/*

  - create autoincrement key
      - https://stackoverflow.com/questions/787722/postgresql-autoincrement
  - get the insert id
      - https://stackoverflow.com/questions/2944297/postgresql-function-for-last-inserted-id
  - Foreign Keys
      - https://www.postgresql.org/docs/8.3/static/tutorial-fk.html

 */

/*
    1. Start a postgresql daemon

         docker run -it --rm -d --name some-postgres postgres:alpine

    2. Start a postgresql cli

         docker run -it --rm --link some-postgres:postgres -v $(pwd):/data postgres:alpine psql -h postgres -U postgres

    3. source the file in pql cli:
        - `\i /data/postgresql.sql`
 */

DROP TABLE IF EXISTS task_logs;
DROP TABLE IF EXISTS todo_tasks;
DROP TYPE  IF EXISTS task_status;

CREATE TYPE task_status AS ENUM('ready', 'running', 'finished', 'timeout', 'fail');

CREATE TABLE todo_tasks (
    id             SERIAL PRIMARY KEY,
    task_uuid      varchar(48),
    input_path     text,
    output_path    text,
    cmd            text,
    options        text            DEFAULT '',
    status         task_status     DEFAULT 'ready',
    timeout        integer         DEFAULT 0,
    machine        varchar(128),
    start_time     timestamp,
    finish_time    timestamp,
    retry_time     integer         DEFAULT 0,
    retry_limit    integer         DEFAULT 1,
    removed        boolean         DEFAULT false
);

CREATE TABLE task_logs (
    id            SERIAL PRIMARY KEY,
    task_id       integer references todo_tasks(id),
    retry_time    integer,
    return_code   integer,
    cmd           text,
    log_data      text,
    start_time    timestamp,
    finish_time   timestamp
);

-- https://stackoverflow.com/questions/2944297/postgresql-function-for-last-inserted-id

-- Insert
INSERT INTO todo_tasks (task_uuid, input_path,output_path) VALUES ('abcd', '123.txt', '456.txt') RETURNING id;
INSERT INTO todo_tasks (task_uuid, input_path,output_path) VALUES ('abcd', '456.txt', '789.txt') RETURNING id;
INSERT INTO todo_tasks (task_uuid, input_path,output_path) VALUES ('abcd', 'abcd.txt', '1234.txt') RETURNING id;
INSERT INTO todo_tasks (task_uuid, input_path,output_path) VALUES ('abcd', 'abcd.txt', '1234.txt') RETURNING id;
INSERT INTO todo_tasks (task_uuid, input_path,output_path) VALUES ('abcd', 'abcd.txt', '1234.txt') RETURNING id;

-- Update
UPDATE todo_tasks SET machine='ubuntu-1', start_time=CURRENT_TIMESTAMP WHERE ID=1;

-- Delete
DELETE FROM todo_tasks WHERE id=5 RETURNING *;
DELETE FROM todo_tasks WHERE id=4 RETURNING id, task_uuid;

-- Transaction
-- Ignroe, try to practice in python
