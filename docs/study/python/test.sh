#!/bin/bash

docker stop some-postgres || true
docker build --rm -t yen3/psql_test .
docker run -it --rm -d -e PGDATA="/var/lib/postgresql/data/pgdata" \
    -v $(pwd)/pgdata:/var/lib/postgresql/data/pgdata \
    --name some-postgres yen3/psql_test

