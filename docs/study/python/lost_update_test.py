#!/usr/bin/env python3

import psycopg2


def main():
    conn = psycopg2.connect(host='postgres', user='postgres')

    try:
        for _ in range(10000):
            with conn.cursor() as cur:
                # Fetch
                cur.execute('SELECT * from counters where id = 1 FOR UPDATE')
                cid, counter = cur.fetchone()

                # Add
                counter = counter + 1

                # Update
                cur.execute('UPDATE counters set counter = ' + str(counter) +
                            ' where id = ' + str(cid))

            conn.commit()

    finally:
        conn.close()


if __name__ == "__main__":
    main()
