    # List
    @schema.validate(output_schema={
        "type": "array",
    )

    # List
    @gen.coroutine
    @schema.validate(output_schema={
        "type": "array",
        "items": {
        "type": "object",
            "properties": {
                "id": {"type": "string"},
                "name": {"type": "string"}
                }
            }
        }
    )
    def get(self):
        """
        List all task ids, if the details is specified. return all task
        informations
        """
        with session_context() as session:
            tasks = session.query(Task).all()
            print("here:", len(tasks))
            session.flush()
            for t in tasks:
                session.expunge(t)
            session.commit()

        result = []
        for task in tasks:
            tid = task.id
            tname = task.name
            result.append({"id": str(tid), "name": tname})

        return result

    # Dict of dict
    @gen.coroutine
    @schema.validate(output_schema={
            "type": "object",
            "additionalProperties" : {
                "type": "object",
                "properties": {
                    "id": {"type": "string"},
                    "name": {"type": "string"},
                }
            }
        }
    )
    def get(self):
        """
        List all task ids, if the details is specified. return all task
        informations
        """
        with session_context() as session:
            tasks = session.query(Task).all()
            print("here:", len(tasks))
            session.flush()
            for t in tasks:
                session.expunge(t)
            session.commit()

        result = {}
        for task in tasks:
            tid = task.id
            tname = task.name
            result[str(tid)] = {"id": str(tid), "name": tname}

        return result
