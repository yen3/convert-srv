#!/usr/bin/env python3

import psycopg2
import uuid

def insert_data():
    try:
        conn = psycopg2.connect(host='postgres', user='postgres')
        task_uuid = 'test_ls'

        for i in range(10):
            cur = conn.cursor()
            i_path = "input_%d.txt" % i
            o_path = "output_%d.txt" % i
            cmd = "ls"
            cmd_options = "-al"

            data = {'task_uuid': task_uuid,
                    'input_path': i_path,
                    'output_path': o_path,
                    'cmd': cmd,
                    'cmd_options': cmd_options}

            keys = list(data.keys())
            values = [str(data[k]) for k in keys]

            value_format = ",".join(["%s" for i in range(len(values))])
            insert_statement_template = "INSERT INTO todo_tasks (%s) VALUES (%s)" % \
                    (",".join(keys), value_format)
            insert_statement = cur.mogrify(insert_statement_template,
                                           tuple(values))
            cur.execute(insert_statement)

            cur.close()

        conn.commit()

    except psycopg2.Error as e:
        print(e)
    finally:
        conn.close()

def lookup_data():
    try:
        conn = psycopg2.connect(host='postgres', user='postgres')
        cur = conn.cursor()
        cur.execute('select * from todo_tasks')
        print(cur.fetchall())
        cur.close()

        conn.commit()

    except psycopg2.Error as e:
        print(e)
    finally:
        conn.close()

#insert_data()
lookup_data()

