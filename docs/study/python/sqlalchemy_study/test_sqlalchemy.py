#!/usr/bin/env python3

from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

URL = "postgres+psycopg2://postgres:@postgres:5432"
ENGINE = create_engine(URL)

Session = sessionmaker(bind=ENGINE)

Tasks = None
Jobs = None
JobLogs = None
Workers = None


def create_class():
    Base = automap_base()
    Base.prepare(ENGINE, reflect=True)

    global Tasks
    global Jobs
    global JobLogs
    global Workers

    Tasks =  Base.classes.tasks
    Jobs = Base.classes.jobs
    JobLogs = Base.classes.job_logs
    Workers = Base.classes.workers


def test_add():
    ws = [Workers(name="test_add_1",addr="abc.gg.org"),
         Workers(name="test_add_2",addr="abc.gg.org"),
         Workers(name="test_add_3",addr="abc.gg.org"),
         Workers(name="test_add_4",addr="abc.gg.org"),
         Workers(name="test_add",addr="abc.gg.org"),
        ]
    session = Session()
    session.add_all(ws)
    session.commit()

    print([w.id for w in ws])


def test_query():
    session = Session()
    # ws = session.query(Workers).filter_by(name='test').all()
    ws = session.query(Workers).order_by(Workers.id).all()
    for w in ws:
        print(w.id, w.name, w.addr)
    session.commit()


def test_update():
    session = Session()
    ws = session.query(Workers).filter_by(name="test_add").first()
    ws.name = "new_convert_test"

    session.commit()


def test_delete():
    session = Session()
    session.add(Workers(name="test_delete",addr="abc.gg.org"))
    session.commit()

    test_query()
    print()

    session = Session()
    del_w = session.query(Workers).filter_by(name="test_delete").first()
    session.delete(del_w)
    session.commit()

    test_query()


def delete_all_rows():
    session = Session()
    ws = session.query(Workers).delete()
    session.commit()

    with ENGINE.connect() as conn:
        rs = conn.execute("SELECT setval('workers_id_seq',1)")



create_class()
test_add()
test_query()
test_update()
test_delete()
delete_all_rows()
