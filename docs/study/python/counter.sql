DROP TABLE IF EXISTS counters;

CREATE TABLE counters (
    id      SERIAL              PRIMARY KEY,
    counter integer  DEFAULT 0
);

INSERT INTO counters (counter) VALUES (0) RETURNING id, counter;
INSERT INTO counters (counter) VALUES (0) RETURNING id, counter;

UPDATE counters set counter = counter + 1 where id = 2 RETURNING counter;
UPDATE counters set counter = counter + 1 where id = 2 RETURNING counter;
UPDATE counters set counter = counter + 1 where id = 2 RETURNING counter;
UPDATE counters set counter = counter + 1 where id = 2 RETURNING counter;
UPDATE counters set counter = counter + 1 where id = 2 RETURNING counter;

DELETE FROM counters where id = 2 RETURNING counter;

-- Transaction for dirty write test
BEGIN;
SELECT id from counters where id = 1 FOR UPDATE;
UPDATE counters set counter = counter + 1 where id = 1;
UPDATE counters set counter = counter + 1 where id = 1;
UPDATE counters set counter = 0 where id = 1;
COMMIT;
